<?php if (!defined('ABSPATH')) die('Direct access forbidden.');
/**
 * customizer option: blog
 */

$options =[
    'blog_settings' => [
        'title'		 => esc_html__( 'Blog settings', 'gloreya' ),

        'options'	 => [
            'blog_sidebar' =>[
                'type'  => 'select',
                              
                'label' => esc_html__('Sidebar', 'gloreya'),
                'desc'  => esc_html__('Description', 'gloreya'),
                'help'  => esc_html__('Help tip', 'gloreya'),
                'choices' => array(
                    '1' => esc_html__('No sidebar','gloreya'),
                    '2' => esc_html__('Left Sidebar', 'gloreya'),
                    '3' => esc_html__('Right Sidebar', 'gloreya'),
                 
                 ),
              
                'no-validate' => false,
            ],   
            'blog_title' => [
                'label'	 => esc_html__( 'Global blog title', 'gloreya' ),
                'type'	 => 'text',
            ],
            'blog_header_image' => [
                'label'	 => esc_html__( 'Global header background image', 'gloreya' ),
                'type'	 => 'upload',
             ],
            'blog_breadcrumb' => [
                'type'			 => 'switch',
                'label'			 => esc_html__( 'Breadcrumb', 'gloreya' ),
                'desc'			 => esc_html__( 'Do you want to show breadcrumb?', 'gloreya' ),
                'value'          => 'yes',
                'left-choice'	 => [
                    'value'	 => 'yes',
                    'label'	 => esc_html__( 'Yes', 'gloreya' ),
                ],
                'right-choice'	 => [
                    'value'	 => 'no',
                    'label'	 => esc_html__( 'No', 'gloreya' ),
                ],
            ],
            'blog_author' => [
                'type'			 => 'switch',
                'label'			 => esc_html__( 'Blog author', 'gloreya' ),
                'desc'			 => esc_html__( 'Do you want to show blog author?', 'gloreya' ),
                'value'          => 'no',
                'left-choice' => [
                    'value'	 => 'yes',
                    'label'	 => esc_html__( 'Yes', 'gloreya' ),
                ],
                'right-choice' => [
                    'value'	 => 'no',
                    'label'	 => esc_html__( 'No', 'gloreya' ),
                ],
           ],
            'blog_social_share' => [
                'type'			 => 'switch',
                'label'			 => esc_html__( 'Social share', 'gloreya' ),
                'desc'			 => esc_html__( 'Do you want to show social share buttons?', 'gloreya' ),
                'value'          => 'no',
                'left-choice' => [
                    'value'	 => 'yes',
                    'label'	 => esc_html__( 'Yes', 'gloreya' ),
                ],
                'right-choice' => [
                    'value'	 => 'no',
                    'label'	 => esc_html__( 'No', 'gloreya' ),
                ],
           ],
        ],
            
        ]
    ];