<?php if( get_theme_mod('bonfire_touchy_hide_menu_button') == '') { ?>
<div class="touchy-menu-button">
    <div class="touchy-menu-tooltip"></div>
    <span class="touchy-menu-text-label-offset">
        <?php if( get_theme_mod('bonfire_touchy_menu_icon') == '') { ?>
            <div class="touchy-default-menu"></div>
        <?php } else { ?>
            <i class="fa <?php echo get_theme_mod('bonfire_touchy_menu_icon'); ?>"></i>
        <?php } ?>
    </span>
</div>
<?php } ?>