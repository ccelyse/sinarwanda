<?php

/**
 * Fired during plugin activation
 *
 * @link       https://themeforest.net/user/webgeniuslab
 * @since      1.0.0
 *
 * @package    Foodmood_Core
 * @subpackage Foodmood_Core/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Foodmood_Core
 * @subpackage Foodmood_Core/includes
 * @author     WebGeniusLab <webgeniuslab@gmail.com>
 */
class Foodmood_Core_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
