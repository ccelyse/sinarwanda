<?php
if (!class_exists('Foodmood_Theme_Helper')) { return; }

$theme_color = esc_attr(Foodmood_Theme_Helper::get_option('theme-custom-color'));
$theme_color_secondary = esc_attr(Foodmood_Theme_Helper::get_option('theme-secondary-color'));
$header_font = Foodmood_Theme_Helper::get_option('header-font');

if (function_exists('vc_map')) {
    vc_map(array(
        'base' => 'wgl_team',
        'name' => esc_html__( 'Team List', 'foodmood' ),
        'description' => esc_html__( 'Show Team Grid', 'foodmood' ),
        'icon' => 'wgl_icon_team',
        'category' => esc_html__( 'WGL Modules', 'foodmood' ),
        'params' => array(
            // GENERAL TAB
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Columns in Row', 'foodmood' ),
                'param_name' => 'posts_per_line',
                'admin_label' => true,
                'value' => array(
                    esc_html__( '1 Column', 'foodmood' ) => '1',
                    esc_html__( '2 Columns', 'foodmood' ) => '2',
                    esc_html__( '3 Columns', 'foodmood' ) => '3',
                    esc_html__( '4 Columns', 'foodmood' ) => '4',
                    esc_html__( '5 Columns', 'foodmood' ) => '5',
                ),
                'std' => '3',
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Team Info Alignment', 'foodmood' ),
                'param_name' => 'info_align',
                'admin_label' => true,
                'value' => array(
                    esc_html__( 'Left', 'foodmood' ) => 'left',
                    esc_html__( 'Center', 'foodmood' ) => 'center',
                    esc_html__( 'Right', 'foodmood' ) => 'right',
                ),
                'edit_field_class' => 'vc_col-sm-3 no-top-padding',
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Gap Between Items', 'foodmood' ),
                'param_name' => 'grid_gap',
                'value' => array(
                    esc_html__( '0px', 'foodmood' ) => '0',
                    esc_html__( '2px', 'foodmood' ) => '2',
                    esc_html__( '4px', 'foodmood' ) => '4',
                    esc_html__( '6px', 'foodmood' ) => '6',
                    esc_html__( '10px', 'foodmood' ) => '10',
                    esc_html__( '20px', 'foodmood' ) => '20',
                    esc_html__( '30px', 'foodmood' ) => '30',
                ),
                'std' => '30',
                'edit_field_class' => 'vc_col-sm-3 no-top-padding',
            ),
            array(
                'type' => 'foodmood_param_heading',
                'param_name' => 'divider_1',
                'edit_field_class' => 'divider',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Add Link for Image', 'foodmood' ),
                'param_name' => 'single_link_wrapper',
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Add Link for Heading', 'foodmood' ),
                'param_name' => 'single_link_heading',
                'value' => 'true',
                'edit_field_class' => 'vc_col-sm-3',
            ),
			array(
				'type' => 'foodmood_param_heading',
				'param_name' => 'divider_2',
				'edit_field_class' => 'divider',
			),
            // Hide title checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Hide Title', 'foodmood' ),
                'param_name' => 'hide_title',
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Hide department checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Hide Department', 'foodmood' ),
                'param_name' => 'hide_department',
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Hide socials checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Hide Social Icons', 'foodmood' ),
                'param_name' => 'hide_soc_icons',
                'edit_field_class' => 'vc_col-sm-3',
            ),
            vc_map_add_css_animation( true ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Extra Class', 'foodmood' ),
                'param_name' => 'item_el_class',
                'description' => esc_html__( 'To customly style particular element, use this field to add a class name and then refer to it fron Custom CSS settings.', 'foodmood' ),
            ),
            // CAROUSEL TAB
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Use Carousel', 'foodmood' ),
                'param_name' => 'use_carousel',
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3 no-top-margin',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Autoplay', 'foodmood' ),
                'param_name' => 'autoplay',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-1 no-top-padding',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Autoplay Speed', 'foodmood' ),
                'param_name' => 'autoplay_speed',
                'value' => '3000',
                'description' => esc_html__( 'Value in milliseconds.', 'foodmood' ),
                'dependency' => array(
                    'element'   => 'autoplay',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3 no-top-padding',
            ),
            array(
                'type' => 'foodmood_param_heading',
                'param_name' => 'divider_ca_1',
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'divider',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Infinite Loop Sliding', 'foodmood' ),
                'param_name' => 'carousel_infinite',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Slide per single item at a time', 'foodmood' ),
                'param_name' => 'scroll_items',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Center Mode', 'foodmood' ),
                'param_name' => 'center_mode',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            // Сarousel pagination style
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Pagination Style', 'foodmood' ),
                'param_name' => 'h_pag_controls',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Add Pagination control', 'foodmood' ),
                'param_name' => 'use_pagination',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            array(
                'type' => 'foodmood_radio_image',
                'heading' => esc_html__( 'Pagination Type', 'foodmood' ),
                'param_name' => 'pag_type',
                'fields' => array(
                    'circle' => array(
                        'image_url' => get_template_directory_uri() . '/img/wgl_composer_addon/icons/pag_circle.png',
                        'label' => esc_html__( 'Circle', 'foodmood' )),
                    'circle_border' => array(
                        'image_url' => get_template_directory_uri() . '/img/wgl_composer_addon/icons/pag_circle_border.png',
                        'label' => esc_html__( 'Empty Circle', 'foodmood' )),
                    'square' => array(
                        'image_url' => get_template_directory_uri() . '/img/wgl_composer_addon/icons/pag_square.png',
                        'label' => esc_html__( 'Square', 'foodmood' )),
                    'line' => array(
                        'image_url' => get_template_directory_uri() . '/img/wgl_composer_addon/icons/pag_line.png',
                        'label' => esc_html__( 'Line', 'foodmood' )),
                    'line_circle' => array(
                        'image_url' => get_template_directory_uri() . '/img/wgl_composer_addon/icons/pag_line_circle.png',
                        'label' => esc_html__( 'Line - Circle', 'foodmood' )),
                ),
                'value' => 'circle',
                'dependency' => array(
                    'element' => 'use_pagination',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Pagination Top Offset', 'foodmood' ),
                'param_name' => 'pag_offset',
                'value' => '',
                'description' => esc_html__( 'Value in pixels.', 'foodmood' ),
                'dependency' => array(
                    'element' => 'use_pagination',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Colors', 'foodmood' ),
                'param_name' => 'custom_pag_color',
                'dependency' => array(
                    'element' => 'use_pagination',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Pagination Color', 'foodmood' ),
                'param_name' => 'pag_color',
                'value' => $theme_color,
                'dependency' => array(
                    'element' => 'custom_pag_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Carousel arrows style
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Arrows Style', 'foodmood' ),
                'param_name' => 'h_arrow_control',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Add Arrows control', 'foodmood' ),
                'param_name' => 'use_prev_next',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Colors', 'foodmood' ),
                'param_name' => 'custom_buttons_color',
                'dependency' => array(
                    'element' => 'use_prev_next',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Arrows Color', 'foodmood' ),
                'param_name' => 'buttons_color',
                'value' => $theme_color,
                'dependency' => array(
                    'element' => 'custom_buttons_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Responsive settings
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Responsive Settings', 'foodmood' ),
                'param_name' => 'h_resp',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Responsive', 'foodmood' ),
                'param_name' => 'custom_resp',
                'dependency' => array(
                    'element' => 'use_carousel',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12 no-top-margin',
            ),
            // Desktop breakpoint
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Desktop Screen Breakpoint', 'foodmood' ),
                'param_name' => 'resp_medium',
                'value' => '1025',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Slides to show', 'foodmood' ),
                'param_name' => 'resp_medium_slides',
                'value' => '',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'foodmood_param_heading',
                'param_name' => 'divider_ca_2',
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'divider',
            ),
            // Tablet breakpoint
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Tablet Screen Breakpoint', 'foodmood' ),
                'param_name' => 'resp_tablets',
                'value' => '800',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Slides to show', 'foodmood' ),
                'param_name' => 'resp_tablets_slides',
                'value' => '',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'foodmood_param_heading',
                'param_name' => 'divider_ca_3',
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'divider',
            ),
            // Mobile breakpoint
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Mobile Screen Breakpoint', 'foodmood' ),
                'param_name' => 'resp_mobile',
                'value' => '480',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Slides to show', 'foodmood' ),
                'param_name' => 'resp_mobile_slides',
                'value' => '',
                'dependency' => array(
                    'element' => 'custom_resp',
                    'value' => 'true',
                ),
                'group' => esc_html__( 'Carousel', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-4',
            ),
            // COLORS TAB
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Background', 'foodmood' ),
                'param_name' => 'h_bg_styles',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12 no-top-margin',
            ),
            // Background color
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Customize Backgrounds', 'foodmood' ),
                'param_name' => 'bg_color_type',
                'value' => array(
                    esc_html__( 'Theme Defaults', 'foodmood' ) => 'def',
                    esc_html__( 'Color', 'foodmood' ) => 'color',
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Background hover color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Idle', 'foodmood' ),
                'param_name' => 'background_color',
                'value' => '#ffffff',
                'dependency' => array(
                    'element' => 'bg_color_type',
                    'value' => 'color'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Hover', 'foodmood' ),
                'param_name' => 'background_hover_color',
                'value' => '#ffffff',
                'dependency' => array(
                    'element' => 'bg_color_type',
                    'value' => 'color'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Title styles heading
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Title', 'foodmood' ),
                'param_name' => 'h_title_styles',
                'group' => esc_html__( 'Colors', 'foodmood' ),
            ),
            // Title color checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Colors', 'foodmood' ),
                'param_name' => 'custom_title_color',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // title color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Title Idle', 'foodmood' ),
                'param_name' => 'title_color',
                'value' => $header_font['color'],
                'dependency' => array(
                    'element' => 'custom_title_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Title Hover', 'foodmood' ),
                'param_name' => 'title_hover_color',
                'value' => $theme_color,
                'dependency' => array(
                    'element' => 'custom_title_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // title styles heading
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Department', 'foodmood' ),
                'param_name' => 'h_depart_styles',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            // title color checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Color', 'foodmood' ),
                'param_name' => 'custom_depart_color',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // title color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Department Color', 'foodmood' ),
                'param_name' => 'depart_color',
                'value' => $theme_color_secondary,
                'dependency' => array(
                    'element' => 'custom_depart_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Title styles heading
            array(
                'type' => 'foodmood_param_heading',
                'heading' => esc_html__( 'Social Icons', 'foodmood' ),
                'param_name' => 'h_soc_styles',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-12',
            ),
            // Title color checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Colors', 'foodmood' ),
                'param_name' => 'custom_soc_color',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Title color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Icon Idle', 'foodmood' ),
                'param_name' => 'soc_color',
                'value' => '#cfd1df',
                'dependency' => array(
                    'element' => 'custom_soc_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Icon Hover', 'foodmood' ),
                'param_name' => 'soc_hover_color',
                'value' => $theme_color,
                'dependency' => array(
                    'element' => 'custom_soc_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'foodmood_param_heading',
                'param_name' => 'divider_co_1',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'divider',
            ),
            // Title color checkbox
            array(
                'type' => 'wgl_checkbox',
                'heading' => esc_html__( 'Customize Backgrounds', 'foodmood' ),
                'param_name' => 'custom_soc_bg_color',
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            // Title color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Idle', 'foodmood' ),
                'param_name' => 'soc_bg_color',
                'value' => '#f3f3f3',
                'dependency' => array(
                    'element' => 'custom_soc_bg_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Hover', 'foodmood' ),
                'param_name' => 'soc_bg_hover_color',
                'value' => '#f3f3f3',
                'dependency' => array(
                    'element' => 'custom_soc_bg_color',
                    'value' => 'true'
                ),
                'group' => esc_html__( 'Colors', 'foodmood' ),
                'edit_field_class' => 'vc_col-sm-3',
            ),
        )
    ));
    Foodmood_Loop_Settings::init('wgl_team', array( 'hide_cats' => true,
                    'hide_tags' => true));
    class WPBakeryShortCode_wgl_Team extends WPBakeryShortCode{}
}