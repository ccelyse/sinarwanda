<?php
class WglThemeHelper{

    protected static $instance = null;

    /**
     * @var \WP_Post
     */
    private $post_id;

    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    } 

    private function __construct () {
        $this->post_id = get_the_ID();
    }

    public function render_post_share ($show_share) {
        $img_url = wp_get_attachment_image_src(get_post_thumbnail_id($this->post_id), 'single-post-thumbnail');

        if ($show_share == "1" || $show_share == "yes") :
        ?>
            <span class="share_title"><?php echo esc_html("Share Post",'foodmood-core') ?></span>
            <!-- post share block -->
            <div class="share_social-wpapper">					
                <a class="share_link share_twitter" target="_blank" href="<?php echo esc_url('https://twitter.com/intent/tweet?text='. get_the_title() .'&amp;url='. get_permalink()); ?>"><span class="fa fa-twitter"></span></a>
                <a class="share_link share_facebook" target="_blank" href="<?php echo  esc_url('https://www.facebook.com/share.php?u='. get_permalink()); ?>"><span class="fa fa-facebook"></span></a>
                <?php
                    if (strlen($img_url[0]) > 0) {
                        echo '<a class="share_link share_pinterest" target="_blank" href="'. esc_url('https://pinterest.com/pin/create/button/?url='. get_permalink() .'&media='. $img_url[0]) .'"><span class="fa fa-pinterest-p"></span></a>';
                    }
                ?>
                <a class="share_link share_linkedin" href="<?php echo esc_url('http://www.linkedin.com/shareArticle?mini=true&url='.substr(urlencode( get_permalink() ),0,1024));?>&title=<?php echo esc_attr(substr(urlencode(html_entity_decode(get_the_title())),0,200));?>" target="_blank" ><span class="fa fa-linkedin"></span></a>
            </div>
            <!-- //post share block -->
        <?php
        endif;
    }

    public function render_post_list_share(){
        ?>
        <div class="share_post-container">
            <a href="#"></a>
            <div class="share_social-wpapper">
                <ul>
                    <li>
                        <a class="share_post share_twitter" target="_blank" href="<?php echo esc_url('https://twitter.com/intent/tweet?text='. get_the_title() .'&amp;url='. get_permalink()); ?>"><span class="fa fa-twitter"></span></a>                
                    </li>
                    <li>
                        <a class="share_post share_facebook" target="_blank" href="<?php echo  esc_url('https://www.facebook.com/share.php?u='. get_permalink()); ?>"><span class="fa fa-facebook"></span></a>            
                    </li> 
                    <?php
                    $img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');
                    if (strlen($img_url[0]) > 0) {
                        echo '<li>';
                        echo '<a class="share_post share_pinterest" target="_blank" href="'. esc_url('https://pinterest.com/pin/create/button/?url='. get_permalink() .'&media='. $img_url[0]) .'"><span class="fa fa-pinterest"></span></a>';
                        echo '</li>';
                    }
                    ?>
                    <li>
                        <a class="share_post share_linkedin" target="_blank" href="<?php echo esc_url('http://www.linkedin.com/shareArticle?mini=true&url='.substr(urlencode( get_permalink() ),0,1024));?>&title=<?php echo esc_attr(substr(urlencode(html_entity_decode(get_the_title())),0,200));?>"><span class="fa fa-linkedin"></span></a>                                    
                    </li>
                </ul>
            </div>
        </div>	    
        <?php
    }

    public static function render_social_shares(){
        $description = esc_html__('Socials', 'foodmood-core');

        $facebook    = Foodmood_Theme_Helper::options_compare('soc_icon_facebook', 'mb_customize_soc_shares', 'on');
        $twitter     =  Foodmood_Theme_Helper::options_compare('soc_icon_twitter', 'mb_customize_soc_shares', 'on');
        $linkedin    = Foodmood_Theme_Helper::options_compare('soc_icon_linkedin', 'mb_customize_soc_shares', 'on');
        $pinterest   = Foodmood_Theme_Helper::options_compare('soc_icon_pinterest', 'mb_customize_soc_shares', 'on');
        $tumblr      = Foodmood_Theme_Helper::options_compare('soc_icon_tumblr', 'mb_customize_soc_shares', 'on');

        $fixed   = Foodmood_Theme_Helper::options_compare('soc_icon_position', 'mb_customize_soc_shares', 'on');
        $offset = Foodmood_Theme_Helper::get_option('soc_icon_offset');
        if (class_exists( 'RWMB_Loader' ) && get_queried_object_id() !== 0) {
            $mb_social_shares = rwmb_meta('mb_customize_soc_shares');
        
            if ($mb_social_shares == 'on') {
                $offset = array();
                $offset['margin-bottom'] = rwmb_meta('mb_soc_icon_offset');
            }
        }   

        $share = Foodmood_Theme_Helper::options_compare('soc_icon_style', 'mb_customize_soc_shares', 'on');
        $share_class = !empty($share) ? " ".$share.'_style' : ' standard_style';
        $share_class .= !empty($fixed) ? " fixed" : '';

        $units = !empty($fixed) ? "%" : 'px';
        $style = '';
        $style .=  isset($offset['margin-bottom']) && $offset['margin-bottom'] !== '' ? ' top:'.(int)$offset['margin-bottom'].$units.';' : '';                

        ?>
        <section class="wgl-social-share_pages<?php echo esc_attr($share_class);?>"<?php
            echo !empty($style) ? ' style="'.$style.'"' : ''
        ?>>
            <div class="share_social-wpapper">
                <?php
                    if($share == 'hovered'):
                ?>
                    <div class="share_social-desc">
                        <div class="share_social-title">
                        <?php
                            echo apply_filters('foodmood_desc_socials', $description);
                        ?>                                
                        </div>
                        <div class="share_social-icon-plus"></div>
                    </div>
                <?php
                    endif;
                ?>
                <ul>
                    <?php
                    if(!empty($twitter)):
                    ?>
                        <li>
                            <a class="share_page share_twitter" target="_blank" href="<?php echo esc_url('https://twitter.com/intent/tweet?text='. get_the_title() .'&amp;url='. get_permalink()); ?>"><span class="fa fa-twitter"></span></a>                
                        </li>
                    <?php
                    endif;
                    ?>

                    <?php
                    if(!empty($facebook)):
                    ?>
                    <li>
                        <a class="share_page share_facebook" target="_blank" href="<?php echo  esc_url('https://www.facebook.com/share.php?u='. get_permalink()); ?>"><span class="fa fa-facebook"></span></a>            
                    </li>

                    <?php
                    endif;
                    ?>  

                    <?php
                    if(!empty($share_pinterest)):
                    ?>     
                    <?php
                    $img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');
                    if (strlen($img_url[0]) > 0) {
                        echo '<li>';
                        echo '<a class="share_page share_pinterest" target="_blank" href="'. esc_url('https://pinterest.com/pin/create/button/?url='. get_permalink() .'&media='. $img_url[0]) .'"><span class="fa fa-pinterest"></span></a>';
                        echo '</li>';
                    }
                    ?>
                    <?php
                    endif;
                    ?>  


                    <?php
                    if(!empty($linkedin)):
                    ?>
                    <li>
                        <a class="share_page share_linkedin" target="_blank" href="<?php echo esc_url('http://www.linkedin.com/shareArticle?mini=true&url='.substr(urlencode( get_permalink() ),0,1024));?>&title=<?php echo esc_attr(substr(urlencode(html_entity_decode(get_the_title())),0,200));?>"><span class="fa fa-linkedin"></span></a>                                    
                    </li>
                    <?php
                    endif;
                    ?>                         

                    <?php
                    if(!empty($tumblr)):
                    ?>
                    <li>
                        <a class="share_page share_tumblr" target="_blank" href="<?php echo esc_url( 'http://www.tumblr.com/share/link?url=' . urlencode(get_permalink()). '&amp;name=' . urlencode(get_the_title()) .'&amp;description='.urlencode(get_the_excerpt()) );?>"><span class="fa fa-tumblr"></span></a>                                    
                    </li>
                    <?php
                    endif;

                    $custom_share = Foodmood_Theme_Helper::get_option('add_custom_share');
                    if(isset($custom_share) && !empty($custom_share)){
                        for ($i = 1; $i <= 10; $i++) {
                            ${'custom_share_'.$i} = Foodmood_Theme_Helper::get_option('select_custom_share_text-'.$i);
                            ${'custom_share_icons'.$i} = Foodmood_Theme_Helper::get_option('select_custom_share_icons-'.$i);
                            
                            if (!empty(${'custom_share_'.$i})) {
                                ?>
                                <li>
                                    <a class="share_page" href="<?php echo esc_url( ${'custom_share_'.$i} );?>"><span class="<?php echo esc_attr(${'custom_share_icons'.$i});?>"></span></a>                                    
                                </li>
                                <?php                                   
                            }
                        }
                    }
                    ?> 
                </ul>
            </div>                
        </section>
        <?php
    }

    public static function render_page_marker(){

        $offset = Foodmood_Theme_Helper::options_compare('page_marker_offset', 'mb_customize_markers', 'on');

        $style =  isset($offset['margin-top']) && $offset['margin-top'] !== '' ? 'top:'.(int)$offset['margin-top'].'%;' : '';           

        ?>
        <section class="wgl-page-markers"<?php echo !empty($style) ? ' style="'.$style.'"' : '' ?>>
            <div class="page-markers-wpapper"><?php
                for ($i = 1; $i <= 6; $i++) {
                    ${'add_marker_'.$i} = Foodmood_Theme_Helper::options_compare('add_marker_'.$i, 'mb_customize_markers', 'on');
                    ${'marker_link_'.$i} = Foodmood_Theme_Helper::options_compare('marker_link_'.$i, 'mb_customize_markers', 'on');
                    ${'marker_color_'.$i} = Foodmood_Theme_Helper::options_compare('marker_color_'.$i, 'mb_customize_markers', 'on');
                    ${'marker_image_'.$i} = Foodmood_Theme_Helper::get_option('marker_image_'.$i);
                    ${'marker_image_alt_'.$i} = isset(${'marker_image_'.$i}['id']) ? get_post_meta(${'marker_image_'.$i}['id'], '_wp_attachment_image_alt', true) : '';
                    ${'marker_image_'.$i} = ${'marker_image_'.$i}['url'];

                    if (class_exists( 'RWMB_Loader' ) && rwmb_meta('mb_customize_markers') == 'on') {
                        ${'mb_marker_image_src'.$i} = rwmb_meta('mb_marker_image_'.$i);
                        if (!empty(${'mb_marker_image_src'.$i})) {
                            ${'marker_image_'.$i} = array_values(${'mb_marker_image_src'.$i});
                            ${'marker_image_alt_'.$i} = isset(${'marker_image_'.$i}[0]['ID']) ? get_post_meta(${'marker_image_'.$i}[0]['ID'], '_wp_attachment_image_alt', true) : '';
                            ${'marker_image_'.$i} = ${'marker_image_'.$i}[0]['full_url'];
                        }
                    }
                    
                    if ((bool)${'add_marker_'.$i}) {?>
                        <a class="page-marker" href="<?php echo esc_url( ${'marker_link_'.$i} );?>" target="_blank">
                            <div class="page-marker_bg"<?php echo isset(${'marker_color_'.$i}) ? ' style="background:'.${'marker_color_'.$i}.';"' : '' ?>></div>
                            <img src="<?php echo esc_attr(${'marker_image_'.$i});?>" alt="<?php echo !empty(${'marker_image_alt_'.$i}) ? esc_attr(${'marker_image_alt_'.$i}) : ''?>"/>
                        </a><?php                                   
                    }
                }?>
            </div>                
        </section>
        <?php
    }

    public static function render_svg_bg1(){
        $svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 94.4 41.9" style="enable-background:new 0 0 94.4 41.9;" preserveAspectRatio="none">
        <path d="M78.1,40.6H15.4c-8,0-14.4-6.5-14.4-14.4V15.9c0-8,6.5-14.4,14.4-14.4h62.7c8,0,14.4,6.5,14.4,14.4v10.2
            C92.5,34.1,86.1,40.6,78.1,40.6z"/>
            <path d="M78.4,41.6c-0.4,0-0.2,0.2-0.3,0.2C78.7,41.8,78.4,41.6,78.4,41.6z"/>
            <polygon points="84.2,39.9 84.2,40 84.9,39.5 					"/>
            <path d="M88.8,36.7c-0.3,0.1-0.7,0.4-1.1,0.8c-0.4,0.4-0.9,0.8-1.3,1.1l-0.2,0L86,39.1c0.5-0.3,1.1-0.8,1.6-1.2
                C88.1,37.4,88.5,37,88.8,36.7z"/>
            <path d="M91.3,34.8c-0.2,0.1,0.3-0.6-0.5,0.6c0.1,0,0.1-0.1,0.1-0.1C91,35.1,91.1,35,91.3,34.8z"/>
            <path d="M90.9,35.2C90.6,35.7,91.3,35,90.9,35.2L90.9,35.2z"/>
            <path d="M92.3,32.7l-0.3,0.8c0.1-0.1,0.5-0.8,0.4-0.5C92.8,32.2,92.6,32.4,92.3,32.7z"/>
            <path d="M92.4,32c0,0.1-0.1,0.3-0.1,0.4C92.3,32.3,92.4,32.2,92.4,32z"/>
            <path d="M92,33.2l0.3-0.8c-0.1,0.1-0.2,0.1-0.1,0.4C92.1,32.6,92.1,32.8,92,33.2z"/>
            <path d="M91.7,33.6c-0.3,0.4-0.6,0.8-0.3,0.1C90.7,35,91,35,91.7,33.6z"/>
            <path d="M88.8,36.3c-0.2,0.1-0.3,0.2-0.5,0.3L88,37C88.2,36.8,88.9,36.3,88.8,36.3z"/>
            <path d="M93.8,27.9c-0.1-0.1-0.2,0.3-0.3,0.3l0,0.6C93.6,28.5,93.7,27.9,93.8,27.9z"/>
            <path d="M93.5,28.2c0-0.3,0-0.6,0-0.9C93.5,28.1,93.4,28.3,93.5,28.2z"/>
            <path d="M92.1,31.7c-0.1,0.5-0.2,0.9-0.4,1.4c0.2-0.6,0.4-1.1,0.5-1.7L92.1,31.7z"/>
            <polygon points="94,24.6 94.1,25.1 93.9,24.2 					"/>
            <path d="M93.9,22.5c0.1,0.3,0.2-0.5,0.3-0.1c0-0.6-0.1-1.1-0.2-1.5C94.1,21.4,94,21.9,93.9,22.5z"/>
            <path d="M93.9,20.2c0,0.2,0.1,0.5,0.1,0.7C94,20.7,93.9,20.4,93.9,20.2z"/>
            <path d="M93.6,25.8c0-0.1,0.1-0.2,0.2-0.7l-0.2,0.3C93.6,25.6,93.6,25.7,93.6,25.8z"/>
            <path d="M93.6,25.5L93.6,25.5c0-0.4,0-0.6,0-0.7C93.6,24.9,93.6,25.1,93.6,25.5z"/>
            <path d="M93.6,25.9C93.6,25.9,93.6,25.9,93.6,25.9C93.6,25.9,93.6,25.9,93.6,25.9z"/>
            <path d="M93,29.6C93,29,93,28,92.7,29C92.8,29.3,93,28.8,93,29.6z"/>
            <path d="M93.4,26.3l-0.1,0.9c0.1,0,0.1-0.2,0.2,0.1L93.4,26.3z"/>
            <path d="M93.3,25.9c0,0.1,0,0.3,0,0.4l0-0.3L93.3,25.9z"/>
            <path d="M93.7,21.7c-0.1-0.4-0.1-0.8-0.1-1.3c0,0.5-0.1,1-0.1,1.5C93.6,22,93.7,22.2,93.7,21.7z"/>
            <path d="M93.6,23.5C93.6,23.5,93.6,23.5,93.6,23.5C93.6,23.5,93.6,23.6,93.6,23.5z"/>
            <polygon points="93.6,15.8 93.5,15.9 93.6,15.9 					"/>
            <path d="M93.5,22c0,0.4,0,0.6,0.1,0.8C93.5,22.5,93.5,22.2,93.5,22C93.5,21.9,93.5,21.9,93.5,22z"/>
            <path d="M93.8,18.9c0,0.2-0.1,0.4-0.1,0.7C93.7,19.3,93.8,19,93.8,18.9z"/>
            <path d="M93.7,23.3L93.7,23.3C93.7,23.3,93.7,23.3,93.7,23.3z"/>
            <path d="M93.7,23.3c0-0.2-0.1-0.3-0.1-0.5c0,0.2,0,0.4,0.1,0.6C93.6,23.4,93.7,23.4,93.7,23.3z"/>
            <path d="M92.3,14.4c0.2,0,0.4,1,0.2,1.9c0.1,0.8,0.7,0.3,1-0.4c-0.1,0-0.1,0-0.3-0.9c0,0.2-0.1,0.7-0.1,1
                c-0.1,0.4-0.1,0.6-0.2,0.2c-0.1-1.2,0-1,0.1-1.5c-0.1,0.3-0.2-0.1-0.3-0.5c-0.2-0.4-0.3-0.8-0.5-0.6l0.3,1
                C92.4,14.5,92.3,14.1,92.3,14.4z"/>
            <path d="M93.5,17.5l0,0.4c0-0.4,0.1-0.7,0.2-1.2c-0.1-0.6-0.1-0.7-0.2-0.8l0,1.8L93.5,17.5z"/>
            <path d="M93.6,19.5c-0.2-0.3-0.3-1.1-0.5-0.6c0,1,0.2,0.7,0.2,1.4C93.3,18.5,93.4,20.7,93.6,19.5
                c0,0.3,0,0.6,0,0.9c0-0.3,0.1-0.6,0.1-0.9c0,0,0,0,0,0l0,0l0,0c0,0.1,0,0.1,0,0C93.6,19.6,93.6,19.6,93.6,19.5
                C93.6,19.6,93.6,19.5,93.6,19.5C93.6,19,93.6,19.4,93.6,19.5c0.1,0.1,0.1,0.1,0.1,0.1l0,0l-0.1-1.7
                C93.5,18.2,93.5,18.7,93.6,19.5z"/>
            <path d="M92.5,28C92.5,27.9,92.5,27.9,92.5,28C92.5,27.8,92.5,27.8,92.5,28z"/>
            <path d="M92.7,27.3c0,0.1,0.1,0.2,0,0.6c-0.1,0.3-0.1,0-0.2,0c0,0.2,0,0.9,0.2,0.1l0-0.1c0.2-0.7,0.4-1.6,0.4-1.5
                C93,26.7,92.8,26.9,92.7,27.3z"/>
            <path d="M93.1,23.3L93,21.9l-0.2,0.7l0.1,0C92.9,23.4,93,23.2,93.1,23.3z"/>
            <polygon points="93.7,15.6 93.9,15.8 93.5,14.9 					"/>
            <polygon points="93.5,13.7 93.7,13.7 93.5,13.2 					"/>
            <path d="M2.2,13.5l0,0.4C2.2,13.7,2.2,13.6,2.2,13.5z"/>
            <path d="M3,10.9L3,10.9C2.9,10.9,2.9,11,3,10.9z"/>
            <path d="M53.7,0.3c-0.6-0.2-1.3-0.4-1.2-0.2l0.2,0.2C53.1,0.3,53.4,0.3,53.7,0.3z"/>
            <path d="M22.9,1.5c0.1,0,0.1,0,0.2,0.1c0.1-0.1,0.3-0.1,0.4-0.2C23.3,1.4,23.1,1.4,22.9,1.5z"/>
            <path d="M22.4,2.3l-0.8-0.1C21.8,2.2,22.1,2.2,22.4,2.3z"/>
            <path d="M2.6,29.4l0.3,0.4C2.7,29.1,2.7,29.4,2.6,29.4z"/>
            <path d="M80,0.5c-0.5-0.2-1-0.2-1.4-0.2C79.3,0.4,79.9,0.7,80,0.5z"/>
            <path d="M0.5,11.9c0,0.3,0,0.4,0,0.4C0.6,12.1,0.6,11.9,0.5,11.9z"/>
            <path d="M77.1,0.3c0.1,0,0.2,0,0.3,0c0.4,0,0.8-0.1,1.3,0C78.1,0.2,77.6,0.2,77.1,0.3z"/>
            <path d="M55.7,39.5c0,0,0.1,0,0.1,0C55.9,39.5,55.8,39.5,55.7,39.5z"/>
            <path d="M73.1,41.4c-0.2,0-0.4,0-0.7,0C72.5,41.4,72.7,41.4,73.1,41.4z"/>
            <path d="M90.5,6.4C90.7,6.7,90.5,6.4,90.5,6.4L90.5,6.4z"/>
            <path d="M91,8.4c-0.1-0.1-0.1-0.1-0.2-0.1C90.9,8.4,90.9,8.4,91,8.4z"/>
            <path d="M73.2,1.5C73.2,1.5,73.2,1.5,73.2,1.5C73.3,1.5,73.3,1.5,73.2,1.5z"/>
            <path d="M90.8,6.9c-0.1-0.1-0.2-0.3-0.3-0.5C90.6,6.5,90.6,6.7,90.8,6.9z"/>
            <path d="M90.8,6.8C90.9,6.9,91,7,91,7.1C91.1,7.1,91.1,7.1,90.8,6.8z"/>
            <path d="M90.1,5.9c0.2,0.2,0.3,0.4,0.4,0.6c0-0.1-0.1-0.1,0-0.1C90.4,6.3,90.3,6.1,90.1,5.9z"/>
            <path d="M23.9,40C23.9,40,24,40,23.9,40C24.4,39.9,24.2,39.9,23.9,40z"/>
            <path d="M43.6,2.3C43.6,2.3,43.6,2.3,43.6,2.3C43.6,2.3,43.6,2.3,43.6,2.3L43.6,2.3z"/>
            <path d="M42.2,2.5c0.6-0.2,0.9-0.2,1.4-0.2l-0.5-0.6L42.2,2.5z"/>
            <path d="M2,20.5l0.2-1c0.3,1.2,0.2,2,0.4,1.1C2,20.5,2.6,16.8,2,16.7l0.1-0.6c0,0.2,0,0.2,0,0.4
                c0.1-0.6,0.1-0.8-0.1-1.1C2,15.1,1.8,14.8,1.9,14c0.1-0.5,0.3-0.9,0.3-0.5c0-0.4,0.1-0.8,0.1-1.2c0.1-0.3,0.2-0.2,0.1,0.1
                c0.1-0.6,0.3-1.1,0.5-1.7l0,0c0-0.3,0.1-0.9,0.2-1.1c0.3-0.4,0,0.2,0.1,0.2c0.1-0.3,0.2-0.6,0.4-1c0.4-0.3,0,0.5-0.3,1.1
                c0.5-0.3,0.5-0.5,0.6-0.8c0.1-0.3,0.3-0.8,1-1.5L4.5,8.5C5.1,8.2,5.4,7.5,6.2,7C6.1,7,5.7,7.3,5.3,7.5C5,7.6,4.8,7.7,5.1,7.3
                c0.2-0.2,0.5-0.5,0.8-1C6,6.4,6.4,6,6.9,5.5l0.1,0.2c0.5-0.5,0.4-0.6,0.7-1c0.7-0.4,1-0.3,1.3-0.3C10.2,4,8,4.6,9.4,3.9
                c0.3-0.1,0.5-0.2,0.7-0.3c0.2-0.1,0.5-0.2,0.7-0.3c0.5-0.2,1-0.4,1.6-0.7c0.8-0.1-0.1,0.2,0.2,0.2l0.7-0.3
                c0.2,0,0.1,0.1,0.1,0.1c0.6-0.2,0-0.3,1-0.5C14.5,2,15,2,15.6,2.1c0.6,0.1,1.4,0.2,2,0c0.3,0.1-0.1,0.2-0.1,0.3
                C19.3,2.2,19,2.3,20.4,2l1.2,0.2c-0.9-0.2,0.2-0.5,1.2-0.7c-0.3-0.1-0.6-0.2-0.8-0.2l1.6-0.2c0.2,0,0,0.1-0.2,0.3
                c0.1,0,0.2,0,0.3-0.1c0.6,0.1,0.3,0.3-0.1,0.4L23,1.5c-0.4,0.2-0.7,0.4-0.7,0.5C22.5,2,22.7,2,22.9,2c-0.1,0-0.2,0.1-0.1,0.1
                c0.1,0,0.2,0,0.5-0.1l0.4,0.3l0.1-0.1l2,0.2c1.5-0.3,2.7-0.8,5.1-0.9c-0.6,0.2-0.1,0.4-0.2,0.6c-0.6-0.3-1.8,0.1-3,0.1
                c0.6,0,0.5,0.2,0.3,0.2l2.1-0.3c0,0.2,0.5,0.2,1,0.3C30.7,2,32.7,2.1,33.6,2c0.1,0.1-0.2,0.3-1.2,0.3C33.5,2.6,33.9,1.7,35,2
                c-0.3,0-0.4,0-0.5,0.1c0.6-0.1,2.1,0,1.7,0.1l-0.3,0c1.7,0.1,5.3,0,5.5-0.4c0,0-0.2,0.5-0.3,0.5l3.1-1.3
                C44,1.4,45,2.1,43.6,2.3c0.4,0,0.9,0.1,1.8,0c-0.3,0-0.7-0.5-0.3-0.5c1.1,0.4,0.7,0.2,2,0.5c-0.4-0.1,0-0.6,0.7-0.6
                c-0.1,0.1,0.1,0.5-0.2,0.5l1.7-0.5c-0.4,0.1-0.1,0.4,0.3,0.6c-0.2-0.1,1.4,0,1.9,0l-0.7-0.1c1.9,0.1,1.8-0.7,3.7-0.7
                c-0.2,0.1-0.6,0.6,0.4,0.7C55.3,2,56.7,1,58.4,0.8L58.7,1l1.3-0.2c-0.8,0.4-3,1.2-4.2,1.5C56.5,2.4,56,2.5,57,2.5
                c0.4,0.1-0.6,0.2-0.9,0.2l2.4,0.1c-0.1-0.3,1.9-0.3,1.8-0.5l-2.6,0.3c-0.1-0.3,1.5-0.8,3.3-0.8c0.4,0.1-0.3,0.5-0.3,0.6
                c0.2-0.1,1.6-0.2,1.8-0.1l-0.7,0.2c1,0.1,1.6-0.4,2.7-0.1c0.4,0,1.2,0.4,1.3,0.2c-0.6-0.3-0.8-1.4-0.3-1.6
                c0.1,0,2.1,0.3,2.5,0.5c0.7,0.3-1,0.6-0.4,0.9C67.5,2.1,67.9,2,68,2c0.4,0.1-0.2,0.3,0.7,0.2c0.2-0.3,1.8,0,0.4-0.3
                c0.9-0.1,0.9,0.1,2,0c-0.3-0.2,0.9-0.7,1.8-0.7c-0.1,0.1,0.1,0.2,0.2,0.2c1.8,0,2.8,0.2,4.5,0.2c0.2,0.1,1,0.2,0.6,0.3
                c0.2,0,0.5-0.1,1-0.1c1,0.3-1.1,0-0.4,0.3C79,2.1,79.3,2,79.7,2c0.4,0,0.9,0,1.3,0c-0.2-0.3-1.7-0.3-2.4-0.2
                c0-0.2,0.3-0.4,0.9-0.5c0.3-0.1,0.6-0.1,1-0.1c0.4,0,0.8,0,1.3,0.1c1.4,0.3,0.5,0.4,0.8,0.4c1.3,0.5,1.8,0.6,2.4,0.7
                c0.3,0,0.5,0.1,0.8,0.3c0.3,0.2,0.7,0.3,1.1,0.8c0.6,0.7-0.2,0.4-0.2,0.7c-0.5-0.3-0.8-0.6-1.2-0.8c-0.3-0.3-0.6-0.5-1-0.6
                c0.3,0.2,0.6,0.3,0.8,0.6c-0.3-0.1-1.1-0.4-1.4-0.6c0.1,0.1,0.5,0.3,0.9,0.5c0.4,0.3,0.9,0.6,1.5,0.9c1.1,0.7,2.1,1.7,2.7,2.6
                c0.3,0.2,0.7,0.7,0.6,0.4c-0.1-0.2-0.3-0.3-0.4-0.5c-0.1-0.3,0.1-0.1,0.3,0.1c0.2,0.2,0.4,0.4,0.2,0c0.6,0.6,1.4,1.3,1.9,2.5
                l-0.1,0.3c0.1-0.1-0.2-0.8-0.5-1.1c0.2,0.2,0.3,0.7,0.5,1.1c0.2,0.5,0.3,0.9,0.6,1.1c-0.1-0.3-0.1-0.6-0.2-0.9
                c0.3,0.4,0.5,0.8,0.8,1.3c0.2,0.5,0.4,1,0.5,1.6c0.1-0.6-0.1-0.2,0-1.1c-0.1,0.4-0.2-0.3-0.5-1c-0.2-0.7-0.5-1.2-0.4-0.6
                c0-0.9-0.4-1.4-0.8-2.5C91.4,7.6,91.5,8,91,7.1c0,0-0.1-0.1-0.2-0.1c0,0,0,0,0-0.1l0,0c0,0,0,0,0,0C90.9,7.2,91,7.4,91,7.5
                c-0.4-0.3-1.3-1.5-1.8-1.8c-0.3-0.4,0.2,0.1,0.1-0.2c-0.2-0.1-0.4-0.3-0.6-0.4c0.1-0.2-0.9-1.3,0.2-0.4
                c-0.3-0.3-0.6-0.6-0.9-0.8C87.7,3.5,87.3,3.2,87,3c-0.3-0.3-0.7-0.4-1-0.6C85.6,2,84.6,1.9,85.1,2c-0.6-0.3-1.2-0.5-1.8-0.7
                l0-0.1c-1.8-0.7-3.9-0.6-5.9-1c-0.6,0-1.2,0.1-2.2,0.1L75,0.2c-0.8,0.1-2.3-0.2-2.6,0c-0.8-0.5-3.7-0.1-5.1-0.2l-0.1,0.2
                c-0.9,0-1.8,0.1-3,0.1l0.1,0.2c-1,0.2-2.5-0.1-4.1-0.1c0.1-0.1,0.5-0.1,0.8,0c-1.9-0.4-4,0.3-5.7,0c-0.4,0.1-0.7,0-1,0
                c0,0,0.1,0,0.1,0c0,0-0.1,0-0.2,0c-0.2,0-0.4-0.1-0.6-0.1c0.2,0,0.3,0.1,0.5,0.1c-0.3,0-0.8,0-1.3,0.1l-0.2-0.2
                c0,0-0.1,0-0.1,0c-0.5-0.1-0.4,0.1-0.1,0.3c-0.2,0-0.5,0.1-0.6,0.1c-0.4-0.3-1.3,0.1-1.8-0.2c-0.3,0.2-2.4,0.1-2.7,0.5
                c0,0-0.2-0.1,0.1-0.1c-0.5,0-1.1-0.1-1.6,0l-0.2-0.3L44.4,1c-0.7-0.2-1-0.3-0.3-0.5c-1.7,0.3-1.4,0.1-2.9,0.4l0.1-0.2
                c-0.5,0-1.4,0.2-1.7,0.1c-1.2-0.3-5.8,0-8.8-0.4c0.6,0.5-1.4-0.2-1.5,0.2c-0.2-0.1-0.6-0.2-0.1-0.3c-1.5,0.1-2.3-0.2-3.4,0.1
                c-0.2-0.1,0.3-0.2,0.2-0.2c-0.1,0-0.5,0.1-0.6,0c-0.2-0.1,0.2-0.1,0.5-0.2c-1.6,0-1.7,0.4-1.8,0.7c-0.8-0.2-1.2-0.1-1.8,0.1
                c-0.3-0.1-0.7-0.2,0.2-0.3c-0.5,0-3.3-0.2-3.4,0.1c-0.2-0.1-0.8,0-0.9,0c-1.2,0-1.3,0-2.5,0l0.3,0c-0.5,0.3-1,0.1-2,0.2l0,0
                c-2.3,0.1-1,0.2-3.2,0.5l0.2,0.2c0,0.3-0.5,0.4-1.1,0.5C9.1,2.4,8.3,2.6,7.8,3l0.5-0.1C8,3.1,7.5,3.2,7,3.4
                C6.5,3.5,5.9,3.7,5.6,3.9C5.5,3.9,5.8,3.7,5.9,3.5c-1.7,1,0,0.4-1.1,1.4C4.6,5,4.8,4.3,4.1,5c-0.2,0.2-0.7,0.7-1,1.5
                C2.6,7.2,2.3,8,1.8,8.5C2,8.3,2.3,8.1,2,8.8c-0.4,0.9-1.6,2.4-1.4,3.4c-0.1,0.1-0.1,0.2-0.1,0.2c-0.1,0.8-0.3,2.5-0.1,3
                C-0.3,16.1,0.8,20,0.1,20l0,2.3l-0.1-0.1c0,1.5,0.1,2,0.3,2.5c0,0.3-0.1,0.1-0.1,0.1c0,0.5-0.1,0.9-0.1,1.3
                c0,0.4,0,0.7,0.1,0.9c0,0.6,0.2,1.1,0.5,2c-0.1,0.1-0.1,0.2-0.2,0.2c0.1,0.1,0.1,0.4,0.2,0.7c0.1,0.4,0.1,0.8,0.3,1.3
                c0.1,0.2,0.2,0.5,0.2,0.8c0.1,0.3,0.2,0.5,0.3,0.8c0.2,0.5,0.6,1,0.9,1.5c0,0-0.1,0-0.2-0.2c0.7,1,1.6,2.3,2.5,3.2
                c0.4,0.5,0.9,0.9,1.3,1.2c0.4,0.3,0.8,0.4,1.1,0.4c0.5,0.2,1,0.5,1.5,0.6c-1.8-1,0.4-0.1-0.3-0.7c0.7,0.3,1.8,1.1,0.9,0.7
                l-0.1-0.1c-0.5,0.3,2.3,1.1,2.2,1.3c2,0.6,4.1,0.6,6,0.6c1.9,0.1,3.8,0.1,5.7,0.1l-0.4-0.3l1.3,0c0.3,0.1,0.5,0.3-0.5,0.2
                c0.5,0.2,1.5-0.2,1.6-0.2c1.8,0.1-0.7,0.4-0.2,0.4l0.8,0l-0.2,0c0.6-0.1,0.7-0.3,1.8-0.3c0.7,0.1,0.4,0.2,0.6,0.2
                c0,0,0.1,0,0.4,0l1.6,0l-0.7-0.3c0.7-0.1,1.5,0,0.8-0.3c1.1,0.4,6.1,0.2,6.8,0.3c1-0.2,2-0.2,3.2-0.2c-0.3-0.1-0.5-0.3,0.6-0.3
                c-0.4,0.6,2,0.3,2.8,0.7c-0.8-0.3,3-0.1,1.8-0.5c0.9,0,0.1,0.2,0.7,0.4c2.1-0.2,4.7,0.1,7.3,0.1c0-0.1-0.4-0.2,0.2-0.3l1.4,0.4
                c0.5,0,0.5-0.4,1.4-0.2c-0.1-0.1-0.3-0.2,0-0.2c5.1,0.4,10.5-0.3,15.7,0.4c-0.7-0.1-0.2-0.2,0.3-0.2c-0.4,0,0-0.2-0.1-0.3
                l2.2,0.3c0.4-0.3,2.3,0,2.6-0.3l-0.7,0.1c0.9-0.3-1.3-0.7-1.2-1.2c-1,0.4-2.7-0.6-4.4,0c-0.4-0.1,0.3-0.1,0.1-0.2
                C70.5,39.9,70,40,69,39.9l0.1,0c-2.7,0-3.8-0.1-6.9-0.3l0.2,0.2c-0.6,0-0.8-0.1-1.1-0.1c-0.7,0.3,1.8,0,1.1,0.4
                c-0.9-0.4-4.6,0-5.2-0.5c-1,0,0.3,0.2-0.7,0.2l-0.3-0.1l-0.3,0.2c-0.7,0-1-0.2-1-0.3c0.2,0,0.5,0,0.6,0c-0.8-0.2-2.4,0.1-2.7,0
                l1,0.1c-2.7,0.1-6.1,0-8.7,0.2l0.2,0.1c-2,0.2-1.6-0.1-3.8,0l0.1,0c-0.4,0.1-1,0.1-1.6,0.1c0.6-0.1-1-0.2-0.3-0.4
                c-2.2,0.2-1.1,0.2-2.5,0.5L37,40c-0.5,0.1-0.5,0.2-1.6,0.3c0.9-0.1-0.7-0.2,0.3-0.4c-2.3-0.4-3.4,0.4-4.5-0.2
                c-1.1,0.5,2,0.3,1.4,0.3c0.5,0.1-0.9,0.3-1.6,0.3c-0.7,0-1.3-0.6-3.1-0.6c0.2,0,0.2,0,0.4,0c-0.7-0.1-1.4,0.2-2.2,0
                c-0.5,0.3-1.1,0-1.2,0.3l1.7-0.1c-1,0.1-1.9,0.5-3.5,0.3c0.1-0.1,0.6-0.2,0.9-0.2c-0.5-0.1-0.9,0.1-1.4,0
                c0.1-0.3,2.1-0.1,3.1-0.3c-0.5-0.1-2,0.3-1.4-0.1c-0.7,0.5-1.8-0.1-3.1,0.3l0.1-0.2c-0.2,0-0.6,0.1-1,0.1l0.7-0.3
                c-1,0.2-2-0.2-2.9-0.1c2,0,0.5,0.3,1.3,0.4c-2.5,0.3-1.5-0.5-4.2-0.5c0.3,0,1.4,0.1,0.8,0.2c-0.6,0-1.2-0.2-1.5-0.3
                c-1.7-0.2,0.2,0.3-0.5,0.3c-0.6-0.4-0.9,0-1.6-0.1l0-0.2c-1-0.3-1.1-0.3-1.2-0.2c-0.1,0.1-0.3,0.1-1.1-0.3c0.3,0,0.6,0,0.8,0
                c-0.3-0.1-0.6-0.2-0.9-0.3l0.3-0.1c-0.5-0.1-0.8-0.3-1.1-0.4c-0.3-0.2-0.5-0.4-1-0.6C8.2,37.5,7.7,37.6,7,37
                c-0.4-0.5,0.4,0.1-0.6-0.7c0.3,0.1,0.6,0.2,1,0.6c0.1,0,0.3,0.1,0.4,0c-0.2-0.3-0.5-0.5-0.8-0.7c-0.3-0.2-0.6-0.5-0.7-0.6
                c0.1,0.1,0.3,0.3,0.3,0.4c-0.5-0.3-1-0.7-1.5-1.1c-0.1-0.3,0.1-0.1,0.3,0c-0.3-0.2-0.5-0.4-0.8-0.8c0.2,0,0.3-0.1,0.9,0.4
                c-0.4-0.4-0.8-0.8-1.1-1.3c0-0.1-0.3-0.8-0.1-0.7c-0.2-0.4-0.5-0.9-0.8-1.4C3,30.6,2.6,30,2.6,29.3c0-0.1-0.1-0.2-0.2-0.4
                c-0.1-0.6-0.1-1.1-0.1-1.7c0-0.6,0-1.2-0.2-2l0.3-0.9c-0.1-1.2-0.4,1.2-0.5-0.8c-0.1-1.4,0.3-0.5,0.4-1
                c-0.2-0.7-0.5-1.9-0.2-3.2C2.1,19.8,2,20.1,2,20.5z M5,7.3C4.9,7.5,4.7,8,4.5,8.1C4.5,7.7,4.7,7.6,5,7.3z M25,1.9
                c-0.2,0.1-0.5,0-0.9,0C24.4,1.8,24.7,1.8,25,1.9z M87.1,3.3L87.1,3.3C86.6,2.8,86.7,3,87.1,3.3z M88.1,4.3c0.1,0.1,0.1,0,0.1,0
                c-0.1,0-0.3-0.2-0.6-0.5C87.7,3.9,87.8,4,88.1,4.3z M28.1,41.4l-0.3-0.1C28.4,41.3,28.2,41.3,28.1,41.4z"/>
            <path d="M22.5,2.2c-0.1,0-0.1-0.1-0.1-0.1C22.3,2.1,22.2,2.1,22.5,2.2z"/>
            <path d="M2.6,29.3L2.6,29.3C2.6,29.4,2.6,29.4,2.6,29.3L2.6,29.3z"/>
            <path d="M68,2.3c-0.3,0-0.4-0.1-0.5-0.1C67.5,2.3,67.6,2.3,68,2.3z"/>
            <path d="M92.7,10.1c-0.1-0.3-0.3-0.6-0.4-0.8c0.2,0.4,0.2,0.6,0.3,0.8C92.6,10,92.6,9.8,92.7,10.1z"/>
            <path d="M92.5,17.8c0,0.1,0,0.3,0,0.4l0.1,0C92.6,17.6,92.6,17.2,92.5,17.8z"/>
            <path d="M92.3,11.1c-0.1,0.1,0.1,0.6,0.3,1.2l0-0.1C92.8,12.4,92.6,11.7,92.3,11.1z"/>
            <path d="M92.6,12.5c0,0.2,0.1,0.4,0.1,0.5C92.9,13.1,92.8,12.8,92.6,12.5z"/>
            <path d="M89.5,5.4c0.2,0.3,0.3,0,0.4,0.2C90.2,5.7,88.3,4.3,89.5,5.4z"/>
            <path d="M91.9,11.8l0.2-0.3l-0.3-0.3C91.8,11.4,91.9,11.6,91.9,11.8z"/>
            <path d="M90,7.5c0.2,0.3,0.3,0.6,0.5,0.8c0,0,0.1,0.1,0.2,0.2C90.4,8,90.1,7.4,90,7.5z"/>
            <path d="M90.7,8.5C90.9,8.8,91,9,91.1,9.2C91,9,90.9,8.7,90.7,8.5z"/>
            <path d="M91.4,10.6l-0.1-0.3l-0.3-0.6C91.2,10,91.4,10.3,91.4,10.6z"/>
            <path d="M81.8,1.7c-0.3-0.1-0.7-0.2-1-0.2c-0.1,0.1,0.2,0.1,0.5,0.2C81.6,1.8,81.9,1.8,81.8,1.7z"/>
            <path d="M84.5,3.4c-0.2-0.2-0.2-0.2-0.3-0.3L84,3.3L84.5,3.4z"/>
            <path d="M81.7,2.4c-0.1,0-1.4-0.1-0.2,0.1C81.6,2.5,82.6,2.7,81.7,2.4z"/>
            <polygon points="66.4,1.6 66.7,1.6 66.6,1.1 					"/>
            <path d="M52.7,2.6C54,2.7,53,2.4,54.1,2.5c0.3-0.1-0.9-0.1-0.6-0.2C52.8,2.4,52.4,2.3,52.7,2.6z"/>
            <path d="M53.5,2.3c0.1,0,0.2,0,0.4-0.1C53.7,2.3,53.6,2.3,53.5,2.3z"/>
            <polygon points="75.9,41.6 76.7,41.7 76.2,41.6 					"/>
            <path d="M77.4,41.3l0.2,0.3l0.5-0.3C77.9,41.3,77.6,41.4,77.4,41.3z"/>
            <path d="M6.8,6.2L6.8,6.2c-0.1-0.1-0.3,0-0.5,0.3L6.8,6.2z"/>
            <path d="M2.4,14.5l0-0.2c-0.1,0.1-0.2,0.2-0.3,0.3L2.4,14.5z"/>
            <path d="M2.6,23.2c0-0.4,0-0.8,0-1.2C2.6,22.9,2.3,23.3,2.6,23.2z"/>
        </svg>';
        return $svg;
    }

}

function wgl_theme_helper() {
	return WglThemeHelper::instance();
}
?>