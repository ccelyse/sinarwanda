<?php
namespace WglAddons\Templates;

use Elementor\Plugin;
use Elementor\Frontend;
use WglAddons\Includes\Wgl_Loop_Settings;
use WglAddons\Includes\Wgl_Elementor_Helper;
use WglAddons\Includes\Wgl_Carousel_Settings;

defined( 'ABSPATH' ) || exit;

/**
* WGL Elementor Products Template
*
*
* @class        WglProducts
* @version      1.0
* @category     Class
* @author       WebGeniusLab
*/

class WglProducts
{

    private static $instance = null;
    public static function get_instance( ) {
        if ( null == self::$instance ) {
            self::$instance = new self( );
        }

        return self::$instance;
    }

    public function cache_query($args = array()){
        
        $args['update_post_term_cache'] = false; // don't retrieve post terms
        $args['update_post_meta_cache'] = false; // don't retrieve post meta
        $k = http_build_query( $args );
        $custom_query = wp_cache_get( $k, 'foodmood_theme' );
        if ( false ===  ($custom_query) ) {
            $custom_query = new \WP_Query( $args );
            if ( ! is_wp_error( $custom_query ) && $custom_query->have_posts() ) {
                wp_cache_set( $k, $custom_query, 'foodmood_theme' );
            }
        }
        
        return $custom_query;       
    }

    public function render( $atts ){

        if ( !class_exists( 'WooCommerce' ) ) {
            return;
        }

        extract($atts);

        list($query_args) = Wgl_Loop_Settings::buildQuery($atts);

        // Add Page to Query
        global $paged;
        if (empty($paged)) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }
        $query_args['paged'] = $paged;

        // New Query

        $query_args['post_type'] = 'product';

        $tax = array();
        $product_catalog_terms  = wc_get_product_visibility_term_ids();
        $product_not_in = array($product_catalog_terms['exclude-from-catalog']);
        if ( ! empty( $product_not_in ) ) {
            
            //If mod_security enabled ajax navigation not working.. 
            if ( $products_navigation !== 'load_more' ) {
                $tax[] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'term_taxonomy_id',
                    'terms'    => $product_not_in,
                    'operator' => 'NOT IN',
                );
            }
        }

        if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
            $orderby_value = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));

            // Get order + orderby args from string
            $orderby_value = explode('-', $orderby_value);
            $orderby = esc_attr($orderby_value[0]);
            $order = ! empty( $orderby_value[1] ) ? $orderby_value[1] : $order;

            $orderby = strtolower( $orderby );
            $order   = strtoupper( $order );

            $ordering_args = WC()->query->get_catalog_ordering_args( $orderby, $order );
            $meta_query    = WC()->query->get_meta_query();

            $query_args['orderby'] = $ordering_args['orderby'];
            $query_args['order'] = $ordering_args['order'];

            if ( $ordering_args['meta_key'] ) {
                $query_args['meta_key']       = $ordering_args['meta_key'];
            }  

            if($_GET['orderby'] === 'price'){
                $query_args['order'] = 'ASC';
            }
        }

        if(!empty($tax)){
           $query_args['tax_query'][] = $tax; 
        }
        
        $query = $this->cache_query($query_args);
            // Render Items products
        $wgl_def_atts = array(
            'query' => $query,
            // General
            'products_layout' => '',
            'products_title' => '',
            'products_subtitle' => '',
            // Content

            'hide_price' => '',
            'hide_raiting' => $hide_raiting,
            'hide_content' => '',
            'content_letter_count' => '',

            'products_columns' => '',
            'items_load'  => $items_load,
            'products_style' => 'grid',
        );

        global $wgl_products_atts;
        $wgl_products_atts = array_merge($wgl_def_atts ,array_intersect_key($atts, $wgl_def_atts));
        ob_start();

        get_template_part('templates/shop/products', 'grid');

        $products_items = ob_get_clean();

        // Render row class
        $row_class = '';

        wp_enqueue_script( 'imagesloaded' ); 

        if ($products_layout == 'masonry' || (bool) $show_filter) {
            //Call Wordpress Isotope
            wp_enqueue_script( 'isotope', WGL_ELEMENTOR_ADDONS_URL . 'assets/js/isotope.pkgd.min.js' );
        }


        // Allowed HTML render
        $allowed_html = array(
            'a' => array(
                'href' => true,
                'title' => true,
            ),
            'br' => array(),
            'em' => array(),
            'strong' => array()
        ); 

        // Options for carousel
        if ($products_layout == 'carousel') {

            switch ($products_columns) {
                case '1-5': $item_grid = '5'; break;
                case '1-6': $item_grid = '6'; break;
                case '6':  $item_grid = '2'; break;
                case '3':  $item_grid = '4'; break;
                case '4':  $item_grid = '3'; break;
                case '12': $item_grid = '1'; break;
                default: $item_grid = '5'; break;
            }

            $carousel_options_arr = array(
                'slide_to_show' => $item_grid,
                'slides_to_scroll' => true,
                'autoplay' => $autoplay,
                'autoplay_speed' => $autoplay_speed,
                'use_pagination' => $use_pagination,
                'use_navigation' => $use_navigation,
                'pag_type' => $pag_type,
                'pag_offset' => $pag_offset,
                'custom_pag_color' => $custom_pag_color,
                'pag_color' => $pag_color,
                'custom_resp' => $custom_resp,
                'resp_medium' => $resp_medium,
                'resp_medium_slides' => $resp_medium_slides,
                'resp_tablets' => $resp_tablets,
                'resp_tablets_slides' => $resp_tablets_slides,
                'resp_mobile' => $resp_mobile,
                'resp_mobile_slides' => $resp_mobile_slides,
                'adaptive_height'   => true
            );


            if ((bool)$use_navigation) {
                $carousel_options_arr['use_prev_next'] = 'true';
            }

            wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array(), false, false);

            $products_items = Wgl_Carousel_Settings::init($carousel_options_arr, $products_items, false);

            $row_class = 'products_carousel';
            if(!empty($blog_title) || !empty($blog_title)){
                $row_class .= ' products_carousel_title-arrow';
            }
        }

        $css_class = '';

        // Row class for grid and massonry
        if ( in_array($products_layout, array('grid', 'masonry')) ) {

            switch ( $products_columns ) {
                case '12': $css_class .= ' columns-1'; break;
                case '5':  $css_class .= ' columns-1-5'; break;
                case '1-6':  $css_class .= ' columns-1-6'; break;
                case '4':  $css_class .= ' columns-3'; break;
                case '2':  $css_class .= ' columns-6'; break;
                case '3':  $css_class .= ' columns-4'; break;
            }
            
            if($products_layout == 'grid' ){
                $row_class .= " fit_rows";
            }        

            if($products_layout == 'grid' && !(bool) $show_filter ){
                $row_class .= " grid";
            }    

            if($products_layout == 'masonry'){
                $row_class .= " masonry";
            }
            $row_class .= ' grid-toggle';

            $row_class .= (bool)$add_animation ? ' appear-animation' : '';
            $row_class .= (bool)$add_animation && !empty($appear_animation) ? ' anim-'.$appear_animation : '';
        }

        if($products_layout == 'masonry' || (bool) $show_filter){
            $row_class .= " isotope";
        }

        $row_class .= " products-style-grid";
        // Render wraper
        if ($query->have_posts()): ?>
            <section class="wgl_cpt_section woocommerce">
                <div class="wgl-products-catalog wgl-products-wrapper products-posts <?php echo esc_attr($css_class); ?>">
                    <?php      

                    ob_start();
                    if ( (bool) $show_filter ) {    
                        $filter_class = ' filter-'.$filter_align;     
                        echo '<div class="products__filter isotope-filter'.esc_attr($filter_class).'">';

                            $data_category = isset($query_args['tax_query']) ? $query_args['tax_query'] : array();
                            $include = array();
                            $exclude = array();
                            $id_list_in = array();
                            $id_list_not_in = array();
                            if (!is_tax()) {
                                if(isset($data_category[0]['taxonomy']) && $data_category[0]['taxonomy'] === 'product_cat'){
                                    if (!empty($data_category) && isset($data_category[0]) && $data_category[0]['operator'] === 'IN') {
                                        foreach ($data_category[0]['terms'] as $key => $value) {
                                            $idObj = get_term_by('slug', $value, 'product_cat'); 
                                            $id_list_in[] = $idObj->term_id;
                                        }
                                        $include = implode(",", $id_list_in);
                                    } elseif (!empty($data_category) && isset($data_category[0]) && $data_category[0]['operator'] === 'NOT IN') {
                                        foreach ($data_category[0]['terms'] as $key => $value) {
                                            $idObj = get_term_by('slug', $value, 'product_cat'); 
                                            $id_list_not_in[] = $idObj->term_id;
                                        }
                                        $exclude = implode(",", $id_list_not_in);
                                    }                               
                                }
         
                            }

                            $cats = get_terms(array(
                                'taxonomy' => 'product_cat',
                                'include' => $include,
                                'exclude' => $exclude,
                                'hide_empty' => true
                            ));
                            echo '<a href="#" data-filter=".item" class="active">'.esc_html__('All','foodmood-core').'<span class="filter_bg"></span></a>';
                            foreach ($cats as $cat) {
                                if($cat->count > 0){                                  
                                    echo '<a href="'.get_term_link($cat->term_id, 'product_cat').'" data-filter=".product_cat-'.$cat->slug.'">';
                                    echo \Foodmood_Theme_Helper::render_html($cat->name);
                                    echo '<span class="filter_bg"></span>';
                                    echo '</a>';
                                }   
                            }

                        echo '</div>'; 
                    }
                    
                    $filter_return = ob_get_clean();

                    if(!empty($products_title) || !empty($products_subtitle)){
                        echo '<div class="wgl_module_title item_title">';
                        if(!empty($products_title)) echo '<h3 class="ebrima_module_title products_title">'.wp_kses( $products_title, $allowed_html ).'</h3>';
                        
                        if((bool) $show_filter && $filter_align === 'after_subtite' && !empty($products_subtitle)){
                            echo '<div class="wgl_module_subtitle-wrapper">';
                        }
                        
                        if(!empty($products_subtitle)) echo '<p class="products_subtitle">'.wp_kses( $products_subtitle, $allowed_html ).'</p>';

                        if((bool) $show_filter && $filter_align === 'after_subtite'){
                                echo \Foodmood_Theme_Helper::render_html($filter_return);
                            }

                        if((bool) $show_filter && $filter_align === 'after_subtite' && !empty($products_subtitle)){
                            echo '</div>';
                        }

                        echo '</div>';           
                    }
                    
                    if((bool) $show_header_products){
                        echo '<div class="wgl-woocommerce-sorting">';     
                            //$item = new Ebrima_Woocoommerce();
                                
                            if((bool) $show_res_count){
                                // Load the template result count.
                                wc_get_template( 'addons/addons-result-count.php', array(
                                    'query' => $query,
                                ) );                        
                            }
                            echo '<div class="wgl-woocommerce-filter">';

                            if((bool) $show_sorting){
                                // Load the template orderby
                                wc_get_template( 'addons/addons-orderby.php', array(
                                    'query' => $query,
                                ) );
                            }                 

                            echo '</div>';
                            
                        echo '</div>';
                    }

                    if((bool) $show_filter && ($filter_align != 'after_subtite' || (empty($products_title) && empty($products_subtitle)))){
                        echo \Foodmood_Theme_Helper::render_html($filter_return);
                    }
                    echo '<div class="wgl-products container-grid row '. esc_attr($row_class) .'">';

                        if ($products_layout == 'carousel' && (bool) $use_navigation) {
                            echo '<div class="carousel_arrows"><span class="left_slick_arrow wgl-slick-arrow"></span><span class="right_slick_arrow wgl-slick-arrow"></span></div>';       
                        }  

                        echo \Foodmood_Theme_Helper::render_html($products_items);
                    echo '</div>';
                    ?>
                </div>
        <?php

        if ( $products_navigation == 'pagination' ) {
            echo \Foodmood_Theme_Helper::pagination('10', $query, $products_navigation_align);
        }

        if ( $products_navigation == 'load_more' ) {
            $wgl_products_atts['post_count'] = $query->post_count;
            $wgl_products_atts['query_args'] = $query_args;
            $wgl_products_atts['atts'] = $atts;
            $class  = '';
            echo \Foodmood_Theme_Helper::load_more($wgl_products_atts, $name_load_more, $class);
        }
            echo '</section>';
        endif;

        // Clear global var
        unset($wgl_products_atts);
    
    }

}