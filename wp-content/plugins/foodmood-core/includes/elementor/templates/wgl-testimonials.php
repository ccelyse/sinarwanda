<?php
namespace WglAddons\Templates;

use Elementor\Plugin;
use Elementor\Frontend;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use WglAddons\Includes\Wgl_Loop_Settings;
use WglAddons\Includes\Wgl_Elementor_Helper;
use WglAddons\Includes\Wgl_Carousel_Settings;
use WglAddons\Includes\Wgl_Icons;

defined( 'ABSPATH' ) || exit;

/**
* WGL Elementor Testimonials Render
*
*
* @class        WglTestimonials
* @version      1.0
* @category     Class
* @author       WebGeniusLab
*/

if (!class_exists('WglTestimonials')) {
    class WglTestimonials
    {

        private static $instance = null;
        public static function get_instance( ) {
            if ( null == self::$instance ) {
                self::$instance = new self( );
            }

            return self::$instance;
        }

        public function render( $self, $atts ){

            $theme_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-custom-color'));
            $header_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('header-font')['color']);

            $carousel_options = array();
            extract($atts);

            if ((bool)$use_carousel) {
                // carousel options array
                $carousel_options = array(
                    'slide_to_show' => $posts_per_line,
                    'autoplay' => $autoplay,
                    'autoplay_speed' => $autoplay_speed,
                    'fade_animation' => $fade_animation,
                    'slides_to_scroll' => true,
                    'infinite' => true,
                    'use_pagination' => $use_pagination,
                    'pag_type' => $pag_type,
                    'pag_offset' => $pag_offset,
                    'pag_align' => $pag_align,
                    'custom_pag_color' => $custom_pag_color,
                    'pag_color' => $pag_color,
                    'use_prev_next' => $use_prev_next, 
                    'prev_next_position' => $prev_next_position,
                    'custom_prev_next_color' => $custom_prev_next_color,
                    'prev_next_color' => $prev_next_color,
                    'prev_next_color_hover' => $prev_next_color_hover,
                    'prev_next_bg_idle' => $prev_next_bg_idle,
                    'prev_next_bg_hover' => $prev_next_bg_hover,
                    'custom_resp' => $custom_resp,
                    'resp_medium' => $resp_medium,
                    'resp_medium_slides' => $resp_medium_slides,
                    'resp_tablets' => $resp_tablets,
                    'resp_tablets_slides' => $resp_tablets_slides,
                    'resp_mobile' => $resp_mobile,
                    'resp_mobile_slides' => $resp_mobile_slides,
                );

                wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array(), false, false);
            }

            $content =  '';


            switch ($posts_per_line) {
                case '1': $col = 12;    break;  
                case '2': $col = 6;     break;
                case '3': $col = 4;     break;
                case '4': $col = 3;     break;
                case '5': $col = '1/5'; break;
            }

            // Wrapper classes
            $self->add_render_attribute( 'wrapper', 'class', [ 'wgl-testimonials', 'type-'.$item_type, ' alignment_'.$item_align  ] );
            if((bool)$hover_animation){
                $self->add_render_attribute( 'wrapper', 'class', 'hover_animation' );
            }

            if((bool) $quote_icon_switcher){
                 $self->add_render_attribute( 'wrapper', 'class', 'add_quote_icon' );
            }

            // Image styles
            $designed_img_width = 140; // define manually
            $image_size = isset($image_size['size']) ? $image_size['size'] : '';
            $image_width_crop = !empty($image_size) ? $image_size*2 : $designed_img_width*2;
            $image_width = 'width: '.(!empty($image_size) ? esc_attr((int)$image_size) : $designed_img_width).'px;';
            
            $testimonials_img_style = $image_width;
            $testimonials_img_style = !empty($testimonials_img_style) ? ' style="'.$testimonials_img_style.'"' : '';

            $name_svg = '<div class="wgl-testimonials_name-flag"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 307.5 66.2" xml:space="preserve" preserveAspectRatio="none">
            <path d="M306.2,29.8c-10.4-1.1-20.1-5.7-30.6-6.2c-0.3,0-0.5,0.1-0.6,0.3c0-1.1,0-2.2,0-3.3c0-3.2,2.2-8.1-1.6-10 c-0.4-0.2-0.9-0.2-1.4-0.4c-1.4-0.6-3.1-1.2-4.6-1.8c-2.1-0.8-4.3-1.2-6.5-1.8c-11.4-3.2-24-4-35.8-4.2c-5.9-0.1-11.8-0.7-17.7-1 C201.2,1.1,195,1.4,188.7,1c-9.4-0.5-18.8-0.8-28.3-0.6c0,0.8-0.1,1.5-0.1,2.3c11,0,22,0.8,33,1.2c22.4,0.9,45.9-0.2,67.8,5.6 c2.5,0.7,4.9,1.8,7.3,2.6c0,0,0,0,0,0c0.2,0,2.8,0.9,2.5,0.7c1.5,1,1.4,2.1,1.5,3.5c0.2,2.1-0.1,4.4-0.1,6.6 c0.1,2.8,0.4,5.5,0,8.3c-0.4,3.3-1.3,6.5-2.1,9.7c-0.5,2.3-0.6,6.1-3.4,6.7c-1.8,0.4-5.3-0.7-7.3-0.9c-2.8-0.3-5.5-0.7-8.3-1 c-11.6-1.1-23.1-2.8-34.7-3.8c-6.2-0.6-12.5-1.3-18.7-1.8c-5.1-0.4-10.2-0.1-15.3-0.3c-5.2-0.2-10.4-0.3-15.6-0.8 c-3.2-0.3-6.4,0-9.5,0.3c-0.1,0.8-0.2,1.7-0.3,2.5c0.4,0,0.8,0,1.1,0c7.1-0.4,14.1,0.8,21.1,1.1c6.6,0.3,13.2-0.1,19.7,0.5
                c12.7,1.2,25.3,2.6,38,4c4,0.4,8,0.8,11.9,1.2c0.5,0,0.8,0.5,0.7,0.9c-0.1,0.6-0.2,1.1-0.3,1.7c-0.4,1.3-0.7,2.6-0.3,3.8
                c0.1,0.2,0.1,0.5,0,0.7c-0.3,0.6-0.3,1.6,0.4,1.9c4.1,2.3,9.4,3.2,14,4.1c6.1,1.1,12.1,2.8,18.3,3.2c2.3,0.2,4.7,0,7,0
                c0.1,0,0.1,0,0.2,0c3.4,0.6,7.9,2,11.4,1.2c1.4-0.3,2.1-1.6,1.5-2.9c-2.3-6-9-15.7-4.8-20.9c2-2.5,5.1-5.2,7.6-7
                c1.5-1,2.7-1.9,2.7-3.8C307.5,30.5,307,29.9,306.2,29.8z M261.5,49.8c-0.7,0.2-1.4,0.4-2.1,0.6c0-0.3,0-0.6-0.3-0.9
                C260,49.6,260.7,49.7,261.5,49.8z M251,54c0.2-0.6,0.3-1.2,0.4-1.9c0.4,0.3,0.8,0.7,1.2,1C252.1,53.4,251.5,53.7,251,54z
                 M252.4,49.6C252.4,49.6,252.4,49.6,252.4,49.6C252.4,49.6,252.4,49.6,252.4,49.6c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.2,0
                c-0.1,0-0.1,0-0.2,0c0-0.3,0-0.6,0.1-0.8c0.2,0,0.3,0,0.5,0.1c0.1,0.3,0.3,0.6,0.6,0.6c0.1,0,0.3,0.1,0.4,0.1
                C253,49.6,252.7,49.6,252.4,49.6z M302.5,33.3c-0.7,0.4-1.3,0.8-1.7,1.1c-1.4,1.2-2.5,2.7-3.8,4c-1.6,1.5-3.4,2.8-3.9,5
                c-1.6,6.2,3.1,13.3,5.5,19.2c-0.2,0.1-0.4,0.2-0.5,0.4c-0.2,0.3-6.4-1.1-7.1-1.1c-4.4-0.1-8.5,0.2-12.9-0.4
                c-4.5-0.7-9-1.8-13.5-2.6c-3-0.5-6.4-1.1-9.6-2.1c-0.7-0.2-0.7-1.2-0.1-1.5c0.3-0.1,0.5-0.2,0.8-0.3c0.2-0.1,0.3-0.1,0.5,0
                c0.3,0.1,0.6,0.2,1,0.2c0.5,0.1,1-0.3,1.2-0.7c0.1-0.2,0.3-0.4,0.5-0.5c3.2-0.9,6.5-1.7,9.2-3.5c0.1-0.1,0.2-0.1,0.3-0.1
                c0.5-0.1,1-0.3,1.4-0.6c3-2.2,3.1-8.9,3.9-12.2c0.9-3.9,1.2-7.5,1.3-11.3c0-0.4,0.3-0.7,0.6-0.8c0.1,0,0.1,0,0.2,0
                c0.2-0.1,0.5-0.1,0.7,0c2.7,1.3,18.3,5,25.8,6.6C303,32,303.1,33,302.5,33.3z"/>
            <path d="M154.4,2.8c2-0.1,3.9-0.1,5.9-0.1c0-0.8,0.1-1.5,0.1-2.3c-3.4,0.1-6.9,0.2-10.3,0.4
                c-0.1,0.7-0.2,1.5-0.2,2.2c0.1,0,0.3,0,0.4,0C151.6,2.9,153,2.8,154.4,2.8z"/>
            <path d="M153.2,39.5c-0.1,0.8-0.3,1.6-0.7,2.2c1.6,0.1,3.2,0.1,4.8,0c0.1-0.8,0.2-1.7,0.3-2.5
                C156.1,39.3,154.6,39.4,153.2,39.5z"/>
            <path d="M268.5,12.1C268.5,12.1,268.5,12.1,268.5,12.1C268.5,12.1,268.5,12.1,268.5,12.1z"/>
            <path d="M0,31.3c-0.1,1.9,1.2,2.8,2.7,3.8c2.6,1.8,5.6,4.5,7.6,7c4.1,5.3-2.5,15-4.8,20.9C5,64.4,5.7,65.7,7,66
                c3.5,0.7,8-0.7,11.4-1.2c0.1,0,0.1,0,0.2,0c2.3,0,4.7,0.2,7,0c6.2-0.5,12.2-2.1,18.3-3.2c4.6-0.8,9.9-1.8,14-4.1
                c0.7-0.4,0.8-1.3,0.4-1.9c-0.1-0.2-0.1-0.4,0-0.7c0.4-1.2,0.1-2.4-0.3-3.8c-0.1-0.5-0.2-1.1-0.3-1.7c-0.1-0.5,0.2-0.9,0.7-0.9
                c4-0.4,8-0.7,11.9-1.2c12.7-1.4,25.3-2.9,38-4c6.6-0.6,13.2-0.2,19.7-0.5c7-0.4,14.1-1.5,21.1-1.1c0.4,0,0.8,0,1.1,0
                c-0.1-0.8-0.2-1.7-0.3-2.5c-3.2-0.2-6.3-0.5-9.5-0.3c-5.2,0.4-10.4,0.5-15.6,0.8c-5.1,0.2-10.2-0.1-15.3,0.3
                c-6.2,0.5-12.5,1.2-18.7,1.8c-11.6,1-23.1,2.7-34.7,3.8c-2.8,0.3-5.5,0.7-8.3,1c-2,0.2-5.5,1.3-7.3,0.9c-2.8-0.6-2.9-4.4-3.4-6.7
                c-0.7-3.2-1.6-6.4-2.1-9.7c-0.4-2.8-0.1-5.5,0-8.3c0.1-2.1-0.3-4.5-0.1-6.6c0.1-1.3,0-2.5,1.5-3.5c-0.2,0.2,2.3-0.7,2.5-0.7
                c0,0,0,0,0,0c2.4-0.8,4.8-1.9,7.3-2.6c21.9-5.8,45.4-4.8,67.8-5.6c11-0.4,22-1.2,33-1.2c0-0.8-0.1-1.5-0.1-2.3
                c-9.5-0.2-19,0.1-28.3,0.6c-6.2,0.3-12.5,0.1-18.7,0.4c-5.9,0.3-11.8,0.9-17.7,1C70.5,2.6,58,3.4,46.5,6.6
                c-2.2,0.6-4.4,1-6.5,1.8c-1.5,0.5-3.2,1.2-4.6,1.8c-0.5,0.2-0.9,0.2-1.4,0.4c-3.8,1.9-1.6,6.8-1.6,10c0,1.1,0,2.2,0,3.3
                c-0.1-0.2-0.3-0.3-0.6-0.3c-10.5,0.5-20.2,5.1-30.6,6.2C0.5,29.9,0,30.5,0,31.3z M48.3,49.5c-0.2,0.2-0.3,0.6-0.3,0.9
                c-0.7-0.2-1.4-0.4-2.1-0.6C46.7,49.7,47.5,49.6,48.3,49.5z M54.9,53.1c0.4-0.3,0.8-0.6,1.2-1c0.1,0.6,0.3,1.2,0.4,1.9
                C56,53.7,55.4,53.4,54.9,53.1z M54.1,49.6c0.1,0,0.3-0.1,0.4-0.1c0.3-0.1,0.5-0.3,0.6-0.6c0.2,0,0.3,0,0.5-0.1
                c0,0.3,0.1,0.5,0.1,0.8c-0.1,0-0.1,0-0.2,0c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0c0,0,0,0,0,0c0,0,0,0,0,0
                C54.7,49.6,54.4,49.6,54.1,49.6z M5.2,31.9c7.5-1.6,23.1-5.3,25.8-6.6c0.2-0.1,0.5-0.1,0.7,0c0.1,0,0.1,0,0.2,0
                c0.4,0,0.6,0.4,0.6,0.8c0.1,3.8,0.4,7.4,1.3,11.3c0.8,3.3,0.9,10,3.9,12.2c0.5,0.3,1,0.5,1.4,0.6c0.1,0,0.2,0.1,0.3,0.1
                c2.7,1.8,6,2.5,9.2,3.5c0.2,0.1,0.4,0.3,0.5,0.5c0.2,0.5,0.6,0.8,1.2,0.7c0.3,0,0.7-0.1,1-0.2c0.2,0,0.3,0,0.5,0
                c0.3,0.1,0.5,0.2,0.8,0.3c0.7,0.3,0.6,1.2-0.1,1.5c-3.1,1-6.6,1.6-9.6,2.1c-4.5,0.8-9,2-13.5,2.6c-4.3,0.6-8.5,0.3-12.9,0.4
                c-0.7,0-6.9,1.4-7.1,1.1c-0.1-0.2-0.3-0.3-0.5-0.4c2.4-5.9,7.1-13,5.5-19.2c-0.6-2.2-2.4-3.5-3.9-5c-1.3-1.2-2.4-2.8-3.8-4
                c-0.4-0.3-1-0.7-1.7-1.1C4.4,33,4.5,32,5.2,31.9z"/>
            <path d="M153.1,2.8c-2-0.1-3.9-0.1-5.9-0.1c0-0.8-0.1-1.5-0.1-2.3c3.4,0.1,6.9,0.2,10.3,0.4c0.1,0.7,0.2,1.5,0.2,2.2
                c-0.1,0-0.3,0-0.4,0C155.9,2.9,154.5,2.8,153.1,2.8z"/>
            <path d="M154.3,39.5c0.1,0.8,0.3,1.6,0.7,2.2c-1.6,0.1-3.2,0.1-4.8,0c-0.1-0.8-0.2-1.7-0.3-2.5
                C151.4,39.3,152.8,39.4,154.3,39.5z"/>
            <path d="M39,12.1C39,12.1,39,12.1,39,12.1C39,12.1,39,12.1,39,12.1z"/>
            </svg></div>';

            $values = (array) $list; 
            $item_data = array();
            foreach ( $values as $data ) {
                $new_data = $data;
                $new_data['thumbnail'] = isset( $data['thumbnail'] ) ? $data['thumbnail'] : '';
                $new_data['quote'] = isset( $data['quote'] ) ? $data['quote'] : '';
                $new_data['author_name'] = isset( $data['author_name'] ) ? $data['author_name'] : '';
                $new_data['author_position'] = isset( $data['author_position'] ) ? $data['author_position'] : '';
                $new_data['link_author'] = isset( $data['link_author'] ) ? $data['link_author'] : '';

                $item_data[] = $new_data;
            }

            foreach ( $item_data as $item_d ) {
                // image styles
                $testimonials_image_src = (aq_resize($item_d['thumbnail']['url'], $image_width_crop, $image_width_crop, true, true, true));

                if ( ! empty( $item_d['link_author']['url'] ) ) {
                    $self->add_render_attribute( 'link_author', 'href', $item_d['link_author']['url'] );

                    if ( $item_d['link_author']['is_external'] ) {
                        $self->add_render_attribute( 'link_author', 'target', '_blank' );
                    }

                    if ( $item_d['link_author']['nofollow'] ) {
                        $self->add_render_attribute( 'link_author', 'rel', 'nofollow' );
                    }
                }

                $link_author = $self->get_render_attribute_string( 'link_author' );
                // outputs
                $name_output = '<'.esc_attr($name_tag).' class="wgl-testimonials_name">';
                    $name_output .= !empty($item_d['link_author']['url']) ? '<a '.implode( ' ', [ $link_author ] ).'>' : '';
                        $name_output .= esc_html($item_d['author_name']).$name_svg;
                    $name_output .= !empty($item_d['link_author']['url']) ? '</a>' : '';
                $name_output .= '</'.esc_attr($name_tag).'>';

                $quote_output = '<'.esc_attr($quote_tag).' class="wgl-testimonials_quote">'.$item_d['quote'].'</'.esc_attr($quote_tag).'>';
                
                $status_output = !empty($item_d['author_position']) ? '<'.esc_attr($position_tag).' class="wgl-testimonials_position">'.esc_html($item_d['author_position']).'</'.esc_attr($position_tag).'>' : '';
                
                
                $image_output = '';

                if (!empty( $testimonials_image_src )) { 
                    $image_output = '<div class="wgl-testimonials_image">';
                        $image_output .= !empty($item_d['link_author']['url']) ? '<a '.implode( ' ', [ $link_author ] ).'>' : '';
                            $image_output .= '<img src="'.esc_url($testimonials_image_src).'" alt="'.esc_attr($item_d['author_name']).' photo" '.$testimonials_img_style.'>';
                        $image_output .= !empty($item_d['link_author']['url']) ? '</a>' : '';
                    $image_output .= '</div>';
                }


                $content .= '<div class="wgl-testimonials-item_wrap'.(!(bool)$use_carousel ? " item wgl_col-".$col : '').'">';
                    switch ($item_type) {
                        case 'author_top':
                            $content .= '<div class="wgl-testimonials_item">';
                                $content .= '<div class="wgl-testimonials-meta_wrap">';
                                    $content .= $image_output;
                                    $content .= '<div class="wgl-testimonials-name_wrap">';
                                        $content .= $name_output;
                                        $content .= $status_output;
                                    $content .= '</div>';
                                $content .= '</div>';
                                $content .= '<div class="wgl-testimonials-content_wrap">';
                                    $content .= $quote_output;
                                $content .= '</div>';
                            $content .= '</div>';
                            break;
                        case 'author_bottom':
                            $content .= '<div class="wgl-testimonials_item">';
                                $content .= '<div class="wgl-testimonials-content_wrap">';
                                    $content .= $quote_output;
                                $content .= '</div>';
                                $content .= '<div class="wgl-testimonials-meta_wrap">';
                                    $content .= '<div class="wgl-testimonials-name_wrap">';
                                        $content .= $name_output;
                                        $content .= $status_output;
                                    $content .= '</div>';
                                    $content .= $image_output;
                                $content .= '</div>';
                            $content .= '</div>';
                            break;
                        case 'inline_top':
                            $content .= '<div class="wgl-testimonials_item">';
                                $content .= '<div class="wgl-testimonials-content_wrap">';
                                    $content .= '<div class="wgl-testimonials-meta_wrap">';
                                        $content .= $image_output;
                                    $content .= '</div>';
                                    $content .= $quote_output;
                                    $content .= '<div class="wgl-testimonials-name_wrap">';
                                        $content .= $name_output;
                                        $content .= $status_output;
                                    $content .= '</div>';    
                                $content .= '</div>';
                            $content .= '</div>';
                            break;
                        case 'inline_bottom':
                            $content .= '<div class="wgl-testimonials_item">';
                                $content .= '<div class="wgl-testimonials-content_wrap">';
                                    $content .= $quote_output;
                                $content .= '</div>';
                                $content .= '<div class="wgl-testimonials-meta_wrap">';
                                    $content .= $image_output;
                                    $content .= '<div class="wgl-testimonials-name_wrap">';
                                        $content .= $name_output;
                                        $content .= $status_output;
                                    $content .= '</div>';
                                $content .= '</div>';
                            $content .= '</div>';
                            break;
                    }
                $content .= '</div>';
            }

            $wrapper = $self->get_render_attribute_string( 'wrapper' );

            $output = '<div  '.implode( ' ', [ $wrapper ] ).'>';
                if((bool)$use_carousel) {
                    $output .= Wgl_Carousel_Settings::init($carousel_options, $content, false);
                }else{
                    $output .= $content;
                }
            $output .= '</div>';

            return $output;
            
        }

    }
}