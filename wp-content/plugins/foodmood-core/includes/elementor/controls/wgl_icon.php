<?php
namespace WglAddons\Controls;

use Elementor\Plugin;
use Elementor\Controls_Manager;
use Elementor\Base_Data_Control;

if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
* Wgl Elementor Custom Icon Control
*
*
* @class        Wgl_Icon
* @version      1.0
* @category Class
* @author       WebGeniusLab
*/

class Wgl_Icon extends Base_Data_Control{

    /**
     * Get radio image control type.
     *
     * Retrieve the control type, in this case `radio-image`.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Control type.
     */
    public function get_type() {
        return 'wgl-icon';
    }

    public function enqueue() {
        // Scripts
        wp_enqueue_script( 'wgl-elementor-extensions', WGL_ELEMENTOR_ADDONS_URL . 'assets/js/wgl_elementor_extenstions.js');

        // Style
        wp_enqueue_style( 'wgl-elementor-extensions', WGL_ELEMENTOR_ADDONS_URL . 'assets/css/wgl_elementor_extenstions.css');
    }

    public static function get_flaticons( ) {
        return array(
            'flaticon-pizza' => 'pizza',
            'flaticon-hand' => 'hand',
            'flaticon-beef' => 'beef',
            'flaticon-onion' => 'onion',
            'flaticon-diet' => 'diet',
            'flaticon-mushroom' => 'mushroom',
            'flaticon-mushroom-1' => 'mushroom-1',
            'flaticon-mushroom-2' => 'mushroom-2',
            'flaticon-tomato' => 'tomato',
            'flaticon-tomato-1' => 'tomato-1',
            'flaticon-tomato-2' => 'tomato-2',
            'flaticon-hamburguer' => 'hamburguer',
            'flaticon-meat' => 'meat',
            'flaticon-cheese' => 'cheese',
            'flaticon-arrow-down-sign-to-navigate' => 'arrow-down-sign-to-navigate',
            'flaticon-star' => 'star',
            'flaticon-question' => 'question',
            'flaticon-cancel' => 'cancel',
            'flaticon-cutlery' => 'cutlery',
            'flaticon-van' => 'van',
            'flaticon-noodle' => 'noodle',
            'flaticon-hotdog' => 'hotdog',
            'flaticon-sausage' => 'sausage',
            'flaticon-wheat' => 'wheat',
            'flaticon-cheers' => 'cheers',
            'flaticon-hop' => 'hop',
            'flaticon-checklist' => 'checklist',
            'flaticon-paper-plane' => 'paper-plane',
            'flaticon-24-hours' => '24-hours',
            'flaticon-customer-service' => 'customer-service',
            'flaticon-envelope' => 'envelope',
            'flaticon-comment-white-oval-bubble' => 'comment-white-oval-bubble',
            'flaticon-heart-black-shape-for-valentines' => 'heart-black-shape-for-valentines',
            'flaticon-apple-logotype' => 'apple-logotype',
            'flaticon-magnifier-tool' => 'magnifier-tool',
            'flaticon-magnifier' => 'magnifier',
            'flaticon-magnifying-glass' => 'magnifying-glass',
            'flaticon-placeholder-filled-point' => 'placeholder-filled-point',
            'flaticon-play-arrow' => 'play-arrow',
            'flaticon-check-mark' => 'check-mark',
            'flaticon-play-button' => 'play-button',
            'flaticon-right-arrow' => 'right-arrow',
            'flaticon-3d-forward-arrow' => '3d-forward-arrow',
            'flaticon-bookmark' => 'bookmark',
            'flaticon-right-arrow-1' => 'right-arrow-1',
            'flaticon-left-arrow' => 'left-arrow',
            'flaticon-magnifying-glass-1' => 'magnifying-glass-1',
            'flaticon-heart' => 'heart',
            'flaticon-user' => 'user',
            'flaticon-email' => 'email',
            'flaticon-shopping-cart' => 'shopping-cart',
            'flaticon-magnifying-glass-2' => 'magnifying-glass-2',
            'flaticon-phone-call' => 'phone-call',
            'flaticon-shopping-bag' => 'shopping-bag',
            'flaticon-play-button-1' => 'play-button-1',
            'flaticon-search' => 'search',
            'flaticon-internet' => 'internet',
            'flaticon-unlink' => 'unlink',
            'flaticon-placeholder' => 'placeholder',
            'flaticon-placeholder-1' => 'placeholder-1',
            'flaticon-menu' => 'menu',
            'flaticon-play-button-2' => 'play-button-2',
            'flaticon-check' => 'check',
            'flaticon-clock' => 'clock',
            'flaticon-bookmark-1' => 'bookmark-1',
            'flaticon-gear' => 'gear',
            'flaticon-location' => 'location',
            'flaticon-development' => 'development',
            'flaticon-briefcase' => 'briefcase',
            'flaticon-location-1' => 'location-1',
            'flaticon-developer' => 'developer',
            'flaticon-coding' => 'coding',
            'flaticon-news' => 'news',
            'flaticon-video-player' => 'video-player',
            'flaticon-statistics' => 'statistics',
            'flaticon-live-news' => 'live-news',
            'flaticon-pencil' => 'pencil',
            'flaticon-clock-1' => 'clock-1',
            'flaticon-messages' => 'messages',
            'flaticon-construction' => 'construction',
            'flaticon-information' => 'information',
            'flaticon-help' => 'help',
            'flaticon-close' => 'close',
        );
    }

    /**
     * Get radio image control default settings.
     *
     *
     * @since 1.0.0
     * @access protected
     *
     * @return array Control default settings.
     */
    protected function get_default_settings() {
        return [
            'label_block' => true,
            'options' => self::get_flaticons(),
            'include' => '',
            'exclude' => '',
            'select2options' => [],
        ];
    }

    /**
     * Render radio image control output in the editor.
     *
     * Used to generate the control HTML in the editor using Underscore JS
     * template. The variables for the class are available using `data` JS
     * object.
     *
     * @since 1.0.0
     * @access public
     */
    public function content_template() {

        $control_uid = $this->get_control_uid();
        ?>
        <div class="elementor-control-field">
            <# if ( data.label ) {#>
                <label for="<?php echo $control_uid; ?>" class="elementor-control-title">{{{ data.label }}}</label>
            <# } #>
            <div class="elementor-control-input-wrapper">
                <select id="<?php echo $control_uid; ?>" class="elementor-control-icon elementor-select2" type="select2"  data-setting="{{ data.name }}" data-placeholder="<?php echo __( 'Select Icon', 'foodmood-core' ); ?>">
                    <# _.each( data.options, function( option_title, option_value ) {
                        var value = data.controlValue;
                        if ( typeof value == 'string' ) {
                            var selected = ( option_value === value ) ? 'selected' : '';
                        } else if ( null !== value ) {
                            var value = _.values( value );
                            var selected = ( -1 !== value.indexOf( option_value ) ) ? 'selected' : '';
                        }
                        #>
                    <option {{ selected }} value="{{ option_value }}">{{{ option_title }}}</option>
                    <# } ); #>
                </select>
            </div>
        </div>
        <# if ( data.description ) { #>
            <div class="elementor-control-field-description">{{{ data.description }}}</div>
        <# } #>
        <?php
    }
}

?>