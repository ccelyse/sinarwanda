<?php
namespace WglAddons\Widgets;

use WglAddons\Includes\Wgl_Icons;
use WglAddons\Includes\Wgl_Loop_Settings;
use WglAddons\Includes\Wgl_Carousel_Settings;
use WglAddons\Templates\WglPricingTable;
use WglAddons\Widgets\Wgl_Button;
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Wgl_Pricing_Table extends Widget_Base {

    public function get_name() {
        return 'wgl-pricing-table';
    }

    public function get_title() {
        return esc_html__('Wgl Pricing Table', 'foodmood-core' );
    }

    public function get_icon() {
        return 'wgl-pricing-table';
    }

    public function get_categories() {
        return [ 'wgl-extensions' ];
    }


    protected function _register_controls() {
        $theme_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-custom-color'));
        $second_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-secondary-color'));
        $third_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-third-color'));
        $header_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('header-font')['color']);
        $main_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('main-font')['color']);


        /*-----------------------------------------------------------------------------------*/
        /*  CONTENT -> GENERAL
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'wgl_pricing_table_section',
            array(
                'label' => esc_html__('General', 'foodmood-core'),
            )
        );

        $this->add_control(
            'pricing_title',
            array(
                'label' => esc_html__('Title', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Enter your title', 'foodmood-core' ),
                'default' => esc_html__( 'Budget Box', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'pricing_cur',
            array(
                'label' => esc_html__('Currency', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Enter your currency', 'foodmood-core' ),
                'default' => esc_html__( '$', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'pricing_price',
            array(
                'label' => esc_html__('Price', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Enter your price', 'foodmood-core' ),
                'default' => esc_html__( '35', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'pricing_quantity',
            array(
                'label' => esc_html__('Quantity unit', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Enter your quantity', 'foodmood-core' ),
                'default' => esc_html__( '/ once', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'pricing_desc',
            array(
                'label' => esc_html__('Description', 'foodmood-core'),
                'type' => Controls_Manager::TEXTAREA,
                'placeholder' => esc_html__( 'Enter your description', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'content',
            array(
                'label' => esc_html__( 'Content', 'foodmood-core' ),
                'type' => Controls_Manager::WYSIWYG,
                'label_block' => true,
            )
        );

        $this->add_control(
            'hover_animation',
            array(
                'label' => esc_html__('Enable hover animation','foodmood-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'On', 'foodmood-core' ),
                'label_off' => esc_html__( 'Off', 'foodmood-core' ),
                'return_value' => 'yes',
                'description' => esc_html__( 'Lift up the item on hover.', 'foodmood-core' ),
            )
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  CONTENT -> BUTTON
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'wgl_pricing_table_button',
            array(
                'label' => esc_html__('Button', 'foodmood-core'),
            )
        );

        $this->add_control(
            'button_title',
            array(
                'label' => esc_html__('Button Text', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__( 'Buy Now', 'foodmood-core' ),
                'label_block' => true,
            )
        );

        $this->add_control(
            'link',
            array(
                'label' => esc_html__('Button Link', 'foodmood-core'),
                'type' => Controls_Manager::URL,
                'label_block' => true,
            )
        );

        $this->add_responsive_control(
            'button_margin',
            [
                'label' => esc_html__( 'Margin', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 20,
                    'right' => 20,
                    'bottom'=> 50,
                    'left'  => 20,
                ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_footer' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> TITLE
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'title_style_section',
            array(
                'label' => esc_html__( 'Title', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'pricing_title_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .pricing_title',
            )
        );

        $this->add_control(
            'custom_title_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff',
                'selectors' => array(
                    '{{WRAPPER}} .pricing_title' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_control(
            'custom_bg_title_color',
            array(
                'label' => esc_html__( 'Background Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .pricing_title-flag svg' => 'fill: {{VALUE}};',
                ),
            )
        );

        $this->add_responsive_control(
            'title_padding',
            [
                'label' => esc_html__( 'Padding', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 10,
                    'right' => 10,
                    'bottom' => 10,
                    'left' => 10,
                ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' => esc_html__( 'Margin', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> PRICE
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'price_style_section',
            array(
                'label' => esc_html__( 'Price', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'pricing_price_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .pricing_price_wrap',
            )
        );

        $this->add_control(
            'custom_price_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .pricing_price_wrap' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .pricing_header:after' => 'background-color: {{VALUE}};',
                ),
            )
        );

        $this->add_responsive_control(
            'price_margin',
            [
                'label' => esc_html__( 'Margin', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_price_wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> DESCRIPTION
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'desc_style_section',
            array(
                'label' => esc_html__( 'Description', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'pricing_desc_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .pricing_desc',
            )
        );

        $this->add_control(
            'custom_desc_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => esc_attr($main_font_color),
                'selectors' => array(
                    '{{WRAPPER}} .pricing_desc' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_responsive_control(
            'desc_margin',
            [
                'label' => esc_html__( 'Margin', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_desc' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> CONTENT
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'content_style_section',
            array(
                'label' => esc_html__( 'Content', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'pricing_content_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .pricing_content',
            )
        );

        $this->add_control(
            'custom_content_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .pricing_content' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_responsive_control(
            'contet_margin',
            [
                'label' => esc_html__( 'Margin', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .pricing_content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> BACKGROUND
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'bg_style_section',
            array(
                'label' => esc_html__( 'Background', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'background',
                'label' => esc_html__( 'Background', 'foodmood-core' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .pricing_plan_wrap',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'bg_border',
                'selector' => '{{WRAPPER}} .pricing_plan_wrap',
            ]
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> BUTTON
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'section_style',
            [
                'label' => esc_html__( 'Button', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography',
                'selector' => '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button',
            ]
        );

        $this->start_controls_tabs( 'tabs_button_style' );

        $this->start_controls_tab(
            'tab_button_normal',
            [
                'label' => esc_html__( 'Normal', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'button_text_color',
            [
                'label' => esc_html__( 'Text Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff',
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'button_background_color',
            [
                'label' => esc_html__( 'Background Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'default' => $theme_color,
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'tab_button_hover',
            [
                'label' => esc_html__( 'Hover', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'button_hover_color',
            [
                'label' => esc_html__( 'Text Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover, {{WRAPPER}} a.elementor-button:focus, {{WRAPPER}} .elementor-button:focus' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'button_background_hover_color',
            [
                'label' => esc_html__( 'Background Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'default' => 'transparent',
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover, {{WRAPPER}} a.elementor-button:focus, {{WRAPPER}} .elementor-button:focus' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'button_hover_border_color',
            [
                'label' => esc_html__( 'Border Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'border_border!' => '',
                ],
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover, {{WRAPPER}} a.elementor-button:focus, {{WRAPPER}} .elementor-button:focus' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'border',
                'selector' => '{{WRAPPER}} .elementor-button',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'button_border_radius',
            [
                'label' => esc_html__( 'Border Radius', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'default' => [
                    'top' => 40,
                    'right' => 40,
                    'bottom'=> 40,
                    'left'  => 40,
                    'unit'  => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'button_box_shadow',
                'selector' => '{{WRAPPER}} .elementor-button',
            ]
        );

        $this->add_responsive_control(
            'text_padding',
            [
                'label' => esc_html__( 'Padding', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 17,
                    'right' => 50,
                    'bottom'=> 17,
                    'left'  => 50,
                ],
                'selectors' => [
                    '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->end_controls_section();

    }

    protected function render(){

        $settings = $this->get_settings_for_display();

        if (!empty($settings['button_title'])) {
            // button options array
            $button_options = array(
                'icon_type' => '',
                'text' => $settings['button_title'],
                'link' => $settings['link'],
                'size' => 'xl',
            );
        }

        $svg = \Foodmood_Theme_Helper::render_svg_flag();
        $title_flag = '<div class="pricing_title-flag">'.$svg.'</div>';

        // Wrapper classes
        $pricing_wrap_classes = (bool)$settings['hover_animation'] ? ' hover-animation' : '';

        // Title output
        $pricing_title_out = !empty($settings['pricing_title']) ? '<h4 class="pricing_title">'.esc_html($settings['pricing_title']).$title_flag.'</h4>' : '';

        // Currency output
        $pricing_cur_out = !empty($settings['pricing_cur']) ? '<span class="pricing_cur">'.esc_html($settings['pricing_cur']).'</span>' : '';

        // Price output
        if (isset($settings['pricing_price'])) {
            preg_match( "/(\d+)(\.| |,)(\d+)$/", $settings['pricing_price'], $matches, PREG_OFFSET_CAPTURE );
            switch (isset($matches[0])) {
                case false:
                    $pricing_price_out = '<div class="pricing_price">'.esc_html($settings['pricing_price']).'</div>';
                    break;
                case true:
                    $pricing_price_out = '<div class="pricing_price">';
                        $pricing_price_out .= esc_html($matches[1][0]);
                        $pricing_price_out .= '<span class="price_decimal">'.esc_html($matches[3][0]).'</span>';
                    $pricing_price_out .= '</div>';
                    break;
            }
        }

        // Price Quantity unit
        $pricing_quantity_unit = !empty($settings['pricing_quantity']) ? '<div class="pricing_quantity-unit">'.esc_html($settings['pricing_quantity']).'</div>' : '';

        // Price description output
        $pricing_desc_out = !empty($settings['pricing_desc']) ? '<div class="pricing_desc">'.esc_html($settings['pricing_desc']).'</div>' : '';

        // Content
        $pricing_content = !empty($settings['content']) ? $settings['content'] : '';

        // Button output
        ob_start();
            echo Wgl_Button::init_button($this, $button_options);
        $pricing_button = ob_get_clean();

        // Render html
        $pricing_inner = '<div class="pricing_header">';
            $pricing_inner .= $pricing_title_out;
            $pricing_inner .= '<div class="pricing_price_wrap">';
                $pricing_inner .= $pricing_cur_out;
                $pricing_inner .= $pricing_price_out;
                $pricing_inner .= $pricing_quantity_unit;
            $pricing_inner .= '</div>';
            $pricing_inner .= $pricing_desc_out;
        $pricing_inner .= '</div>';
        $pricing_inner .= '<div class="pricing_content">';
            $pricing_inner .= $pricing_content;
        $pricing_inner .= '</div>';
        $pricing_inner .= '<div class="pricing_footer">';
            $pricing_inner .= !empty($settings['button_title']) ? $pricing_button : '';
        $pricing_inner .= '</div>';


        $output = '<div class="wgl-pricing_plan'.esc_attr($pricing_wrap_classes).'">';
            $output .= '<div class="pricing_plan_wrap">';
                $output .= $pricing_inner;
            $output .= '</div>';
        $output .= '</div>';

        echo \Foodmood_Theme_Helper::render_html($output);

    }

}