<?php
namespace WglAddons\Widgets;

use WglAddons\Includes\Wgl_Icons;
use WglAddons\Includes\Wgl_Carousel_Settings;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Control_Media;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Repeater;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Wgl_Time_Line_Vertical extends Widget_Base {

    public function get_name() {
        return 'wgl-time-line-vertical';
    }

    public function get_title() {
        return esc_html__('Wgl Time Line Vertical', 'foodmood-core' );
    }

    public function get_icon() {
        return 'wgl-time-line-vertical';
    }

    public function get_categories() {
        return [ 'wgl-extensions' ];
    }

    public function get_script_depends() {
        return [
            'appear',
        ];
    }

    // Adding the controls fields for the premium title
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        $theme_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-custom-color'));
        $second_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-secondary-color'));
        $third_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-third-color'));
        $header_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('header-font')['color']);
        $main_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('main-font')['color']);

        /* Start General Settings Section */
        $this->start_controls_section('wgl_time_line_section',
            array(
                'label' => esc_html__('General Settings', 'foodmood-core'),
            )
        );

        $this->add_control(
            'add_appear',
            array(
                'label' => esc_html__('Add Appear Animation','foodmood-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'On', 'foodmood-core' ),
                'label_off' => esc_html__( 'Off', 'foodmood-core' ),
                'return_value' => 'yes',
                'default' => 'yes',
            )
        );

        $this->add_control(
            'line_color',
            array(
                'label' => esc_html__( 'Line Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#c9c9c9',
                'selectors' => array(
                    '{{WRAPPER}} .wgl-timeline-vertical' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_section();

        /* Start General Settings Section */
        $this->start_controls_section('content_section',
            array(
                'label' => esc_html__('Content Settings', 'foodmood-core'),
            )
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'subtitle',
            array(
                'label' => esc_html__('Subtitle', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '01', 'foodmood-core' ),
				'placeholder' => esc_html__( 'Subtitle', 'foodmood-core' ),
            )
        );

        $repeater->add_control(
            'title',
            array(
                'label' => esc_html__('Title', 'foodmood-core'),
                'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'This is the heading​', 'foodmood-core' ),
				'placeholder' => esc_html__( 'This is the heading​', 'foodmood-core' ),
            )
        );

        $repeater->add_control(
            'time_line_icon_type',
            array(
                'label' => esc_html__('Add Icon/Image', 'foodmood-core'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    '' => [
                        'title' => esc_html__('None', 'foodmood-core'),
                        'icon' => 'fa fa-ban',
                    ],
                    'font' => [
                        'title' => esc_html__('Icon', 'foodmood-core'),
                        'icon' => 'fa fa-smile-o',
                    ],
                    'image' => [
                        'title' => esc_html__('Image', 'foodmood-core'),
                        'icon' => 'fa fa-picture-o',
                    ]
                ],
                'default' => '',
            )
        );

        $repeater->add_control(
            'time_line_icon_pack',
            array(
                'label' => esc_html__('Icon Pack', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'fontawesome' => esc_html__('Fontawesome', 'foodmood-core'),
                    'flaticon' => esc_html__('Flaticon', 'foodmood-core'),
                ],
                'default' => 'fontawesome',
                'condition' => [
                    'time_line_icon_type'  => 'font',
                ]
            )
        );

        $repeater->add_control(
            'time_line_icon_flaticon',
            array(
                'label' => esc_html__( 'Icon', 'foodmood-core' ),
                'type' => 'wgl-icon',
                'label_block' => true,
                'condition' => [
                    'time_line_icon_pack'  => 'flaticon',
                    'time_line_icon_type'  => 'font',
                ],
                'description' => esc_html__( 'Select icon from Flaticon library.', 'foodmood-core' ),
            )
        );

        $repeater->add_control(
            'time_line_icon_fontawesome',
            array(
                'label' => esc_html__( 'Icon', 'foodmood-core' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'condition' => [
                    'time_line_icon_pack'  => 'fontawesome',
                    'time_line_icon_type'  => 'font',
                ],
                'description' => esc_html__( 'Select icon from Fontawesome library.', 'foodmood-core' ),
            )
        );

        $repeater->add_control(
            'time_line_icon_thumbnail',
            array(
                'label' => esc_html__( 'Image', 'foodmood-core' ),
                'type' => Controls_Manager::MEDIA,
                'label_block' => true,
                'condition' => [
                    'time_line_icon_type' => 'image',
                ],
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
            )
        );

        $repeater->add_control(
            'content',
            array(
                'label' => esc_html__('Content', 'foodmood-core'),
                'type' => Controls_Manager::WYSIWYG,
                'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio, neque qui velit. Magni dolorum quidem ipsam eligendi, totam, facilis laudantium cum accusamus ullam voluptatibus commodi numquam, error, est. Ea, consequatur.', 'foodmood-core'),
            )
        );

        $repeater->add_control(
            'date',
            array(
                'label' => esc_html__('Date', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
				'default' => '',
            )
        );

        $this->add_control(
            'items',
            array(
                'label' => esc_html__( 'Layers', 'foodmood-core' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    ['title' => esc_html__('This is the heading​', 'foodmood-core')],
                    ['title' => esc_html__('This is the heading​', 'foodmood-core')],
                ],
                'title_field' => '{{title}}',
            )
        );

        $this->end_controls_section();

        /*-----------------------------------------------------------------------------------*/
        /*  Style Section
        /*-----------------------------------------------------------------------------------*/

        //Title Styles

        $this->start_controls_section(
            'title_style_section',
            array(
                'label' => esc_html__( 'Title', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'title_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .time_line-title',
            )
        );

        $this->start_controls_tabs(
            'title_colors'
        );

        $this->start_controls_tab(
            'title_colors_normal',
            [
                'label' => esc_html__( 'Normal', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'title_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-title' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_colors_hover',
            [
                'label' => esc_html__( 'Hover', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'title_hover_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-item:hover .time_line-title' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        //SubTitle Styles

        $this->start_controls_section(
            'subtitle_style_section',
            array(
                'label' => esc_html__( 'Subtitle', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'subtitle_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .time_line-subtitle',
            )
        );

        $this->start_controls_tabs(
            'subtitle_colors'
        );

        $this->start_controls_tab(
            'subtitle_colors_normal',
            [
                'label' => esc_html__( 'Normal', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'subtitle_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-subtitle' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'subtitle_colors_hover',
            [
                'label' => esc_html__( 'Hover', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'subtitle_hover_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-item:hover .time_line-subtitle' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        // Content Styles

        $this->start_controls_section(
            'content_style_section',
            array(
                'label' => esc_html__( 'Content', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'content_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .time_line-text',
            )
        );

        $this->add_control(
            'content_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $main_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-text' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_control(
            'content_bg_color',
            array(
                'label' => esc_html__( 'Background Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => array(
                    '{{WRAPPER}} .time_line-content' => 'background-color: {{VALUE}};',
                ),
            )
        );

        $this->add_control(
            'content_border_radius',
            array(
                'label' => esc_html__( 'Border Radius', 'foodmood-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
                    'unit'  => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .time_line-content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            )
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'content_border',
				'selector' => '{{WRAPPER}} .time_line-content',
			]
        );

        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'content_shadow',
				'selector' => '{{WRAPPER}} .time_line-content',
			]
		);

        $this->end_controls_section();

        // Date Styles

        $this->start_controls_section(
            'date_style_section',
            array(
                'label' => esc_html__( 'Date', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'date_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .time_line-date',
            )
        );

        $this->start_controls_tabs(
            'date_colors'
        );

        $this->start_controls_tab(
            'date_colors_normal',
            [
                'label' => esc_html__( 'Normal', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'date_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#e2e3e6',
                'selectors' => array(
                    '{{WRAPPER}} .time_line-date' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'date_colors_hover',
            [
                'label' => esc_html__( 'Hover', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'date_hover_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-item:hover .time_line-date' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        // Media Styles

        $this->start_controls_section(
            'media_style_section',
            array(
                'label' => esc_html__( 'Media', 'foodmood-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->start_controls_tabs(
            'media_colors'
        );

        $this->start_controls_tab(
            'media_colors_normal',
            [
                'label' => esc_html__( 'Normal', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'media_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#959595',
                'selectors' => array(
                    '{{WRAPPER}} .time_line-icon' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'media_colors_hover',
            [
                'label' => esc_html__( 'Hover', 'foodmood-core' ),
            ]
        );

        $this->add_control(
            'media_hover_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .time_line-item:hover .time_line-icon' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .time_line-item:hover .time_line-pointer:before' => 'background-color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

    }

    protected function render() {

        wp_enqueue_script('appear', get_template_directory_uri() . '/js/jquery.appear.js', array(), false, false);

        $settings = $this->get_settings_for_display();

        // HTML tags allowed for rendering
        $allowed_html = array(
            'a' => array(
                'href' => true,
                'title' => true,
            ),
            'br' => array(),
            'em' => array(),
            'strong' => array(),
            'span' => array(
                'class' => true,
                'style' => true,
            ),
            'p' => array(
                'class' => true,
                'style' => true,
            )
        );

        $this->add_render_attribute( 'timeline-vertical', [
			'class' => [
                'wgl-timeline-vertical',
                ((bool)$settings['add_appear'] ? 'appear_anim' : ''),
            ],
        ] );

        ?>
        <div <?php echo $this->get_render_attribute_string( 'timeline-vertical' ); ?>>
        <div class="time_line-items_wrap"><?php

        foreach ( $settings['items'] as $index => $item ) {

            $title = $this->get_repeater_setting_key( 'title', 'items' , $index );
            $this->add_render_attribute( $title, [
                'class' => [
                    'time_line-title',
                ],
            ] );

            $item_wrap = $this->get_repeater_setting_key( 'item_wrap', 'items' , $index );
            $this->add_render_attribute( $item_wrap, [
                'class' => [
                    'time_line-item',
                    $item['time_line_icon_type'] == '' ? 'no-image' : '',
                ],
            ] );

            ?>
            <div <?php echo $this->get_render_attribute_string( $item_wrap ); ?>>
                <div class="time_line-pointer"></div>
                <div class="time_line-curve"></div>
                <div class="time_line-content"><?php
                    // Tab Icon/image
                    if($item['time_line_icon_type'] != ''){
                        if ($item['time_line_icon_type'] == 'font' && (!empty($item['time_line_icon_flaticon']) || !empty($item['time_line_icon_fontawesome']))) {
                            switch ($item['time_line_icon_pack']) {
                                case 'fontawesome':
                                    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
                                    $icon_font = $item['time_line_icon_fontawesome'];
                                    break;
                                case 'flaticon':
                                    wp_enqueue_style('flaticon', get_template_directory_uri() . '/fonts/flaticon/flaticon.css');
                                    $icon_font = $item['time_line_icon_flaticon'];
                                    break;
                            }
                            ?>
                            <span class="time_line-icon">
                                <i class="icon <?php echo esc_attr($icon_font) ?>"></i>
                            </span>
                            <?php
                        }
                        if ($item['time_line_icon_type'] == 'image' && !empty($item['time_line_icon_thumbnail'])) {
                            if ( ! empty( $item['time_line_icon_thumbnail']['url'] ) ) {
                                $this->add_render_attribute( 'thumbnail', 'src', $item['time_line_icon_thumbnail']['url'] );
                                $this->add_render_attribute( 'thumbnail', 'alt', Control_Media::get_image_alt( $item['time_line_icon_thumbnail'] ) );
                                $this->add_render_attribute( 'thumbnail', 'title', Control_Media::get_image_title( $item['time_line_icon_thumbnail'] ) );
                                ?>
                                <span class="time_line-icon time_line-icon_image">
                                <?php
                                    echo Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'time_line_icon_thumbnail' );
                                ?>
                                </span>
                                <?php
                            }
                        }
                    }
                    // End Tab Icon/image
                    if (!empty($item['content']) || !empty($item['title'])) {?>
                        <div class="time_line-title_wrap"><?php
                            if (!empty($item['subtitle'])) {?>
                                <div class="time_line-subtitle"><?php echo $item['subtitle'] ?></div><?php
                            }
                            if (!empty($item['title'])) {?>
                                <h3 <?php echo $this->get_render_attribute_string( $title ); ?>><?php echo $item['title'] ?></h3><?php
                            }?>
                        </div><?php
                        if (!empty($item['content'])){?>
                            <div class="time_line-text"><?php echo wp_kses( $item['content'], $allowed_html );?></div><?php
                        }
                    }?>
                </div><?php
                if (!empty($item['date'])){?>
                    <h4 class="time_line-date"><?php echo $item['date'] ?></h4><?php
                }?>
            </div><?php
        }?>
        </div>
        </div><?php
    }

}