<?php
namespace WglAddons\Widgets;

use WglAddons\Includes\Wgl_Loop_Settings;
use WglAddons\Includes\Wgl_Carousel_Settings;
use WglAddons\Templates\WglProducts;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Wgl_Products extends Widget_Base {

    public function get_name() {
        return 'wgl-products';
    }

    public function get_title() {
        return esc_html__('Wgl Products', 'foodmood-core');
    }

    public function get_icon() {
        return 'wgl-products';
    }

    public function get_script_depends() {
        return [
            'slick',
            'jarallax',
            'jarallax-video',
            'imagesloaded',
            'isotope',
            'wgl-elementor-extensions-widgets',
        ];
    }

    public function get_categories() {
        return [ 'wgl-extensions' ];
    }

    // Adding the controls fields for the premium title
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        $theme_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-custom-color'));
        $theme_secondary_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-secondary-color'));
        $main_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('main-font')['color']);
        $header_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('header-font')['color']);


        /*-----------------------------------------------------------------------------------*/
        /*  CONTENT -> SETTINGS
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'wgl_products_section',
            array(
                'label' => esc_html__('Settings', 'foodmood-core'),
            )
        );

        $this->add_control(
            'products_title',
            array(
                'label' => esc_html__('Title', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            )
        );

        $this->add_control(
            'products_subtitle',
            array(
                'label' => esc_html__('Sub Title', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            )
        );

        $this->add_control(
            'products_columns',
            array(
                'label' => esc_html__('Grid Columns Amount', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    '12' => esc_html__('One', 'foodmood-core'),
                    '6'  => esc_html__('Two', 'foodmood-core'),
                    '4'  => esc_html__('Three', 'foodmood-core'),
                    '3'  =>esc_html__('Four', 'foodmood-core'),
                    '1-5'  =>esc_html__('Five', 'foodmood-core'),
                    '1-6'  =>esc_html__('Six', 'foodmood-core'),
                ),
                'default' => '3',
                'tablet_default' => 'inherit',
                'mobile_default' => '12',
                'frontend_available' => true,
                'label_block'  => true,
            )
        );

        $this->add_control(
            'products_layout',
            array(
                'label' => esc_html__('Layout', 'foodmood-core'),
                'type' => 'wgl-radio-image',
                'options' => [
                    'grid' => [
                        'title'=> esc_html__('Grid', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/layout_grid.png',
                    ],
                    'masonry' => [
                        'title'=> esc_html__('Masonry', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/layout_masonry.png',
                    ],
                    'carousel' => [
                        'title'=> esc_html__('Carousel', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/layout_carousel.png',
                    ],
                ],
                'default' => 'grid',
            )
        );

        $this->add_control(
            'show_header_products',
            array(
                'label' => esc_html__('Show Header Shop', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
            )
        );

        $this->add_control(
            'show_res_count',
            array(
                'label' => esc_html__('Show Result Count', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
                'condition' => [
                    'show_header_products' => 'yes'
                ]
            )
        );

        $this->add_control(
            'show_sorting',
            array(
                'label' => esc_html__('Show Default Sorting', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
                 'condition' => [
                    'show_header_products' => 'yes'
                ]
            )
        );

        $this->add_control(
            'show_filter',
            array(
                'label' => esc_html__('Show Filter', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
                'condition' => [
                    'products_layout' => array( 'grid', 'masonry' )
                ]
            )
        );

        $this->add_control(
            'filter_align',
            array(
                'label' => esc_html__('Filter Align', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'left' => esc_html__('Left', 'foodmood-core'),
                    'right' => esc_html__('Right', 'foodmood-core'),
                    'center' => esc_html__('Сenter', 'foodmood-core'),
                ],
                'default' => 'center',
                'condition' => [
                    'show_filter'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'add_animation',
            array(
                'label' => esc_html__('Add Appear Animation', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
            )
        );

        $this->add_control(
            'appear_animation',
            array(
                'label' => esc_html__('Animation Style', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'fade-in' => esc_html__('Fade In', 'foodmood-core'),
                    'slide-top' => esc_html__('Slide Top', 'foodmood-core'),
                    'slide-bottom' => esc_html__('Slide Bottom', 'foodmood-core'),
                    'slide-left' => esc_html__('Slide Left', 'foodmood-core'),
                    'slide-right' => esc_html__('Slide Right', 'foodmood-core'),
                    'zoom' => esc_html__('Zoom', 'foodmood-core'),
                ],
                'default' => 'fade-in',
                'condition' => [
                    'add_animation'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'products_navigation',
            array(
                'label' => esc_html__('Navigation Type', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'none' => esc_html__('None', 'foodmood-core'),
                    'pagination' => esc_html__('Pagination', 'foodmood-core'),
                    'load_more' => esc_html__('Load More', 'foodmood-core'),
                ],
                'default' => 'none',
                'condition' => [
                    'products_layout' => array('grid', 'masonry')
                ]
            )
        );

        $this->add_control(
            'products_navigation_align',
            array(
                'label' => esc_html__('Navigation\'s Alignment', 'foodmood-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'left' => esc_html__('Left', 'foodmood-core'),
                    'center' => esc_html__('Center', 'foodmood-core'),
                    'right' => esc_html__('Right', 'foodmood-core'),
                ],
                'default' => 'left',
                'condition' => [
                    'products_navigation' => 'pagination'
                ]
            )
        );

        $this->add_control(
            'items_load',
            array(
                'label' => esc_html__('Items to be loaded', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('4', 'foodmood-core'),
                'condition' => [
                    'products_navigation' => 'load_more',
                    'products_layout' => array('grid', 'masonry')
                ]
            )
        );

        $this->add_control(
            'name_load_more',
            array(
                'label' => esc_html__('Button Text', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('Load More', 'foodmood-core'),
                'condition' => [
                    'products_navigation' => 'load_more',
                    'products_layout' => array('grid', 'masonry')
                ]
            )
        );

        $this->add_control(
            'spacer_load_more',
            array(
                'label' => esc_html__('Button Spacer Top', 'foodmood-core'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', 'em', 'rem', 'vw' ],
                'range' => [
                    'px' => [ 'min' => -20, 'max' => 200 ],
                ],
                'condition' => [
                    'products_navigation' => 'load_more',
                    'products_layout' => array('grid', 'masonry')
                ],
                'default' => [ 'size' => '20', 'unit' => 'px' ],
                'selectors' => [
                    '{{WRAPPER}} .load_more_wrapper' => 'margin-top: {{SIZE}}{{UNIT}};',
                ],
            )
        );

        $this->end_controls_section();

        
        /*-----------------------------------------------------------------------------------*/
        /*  CONTENT -> DISPLAY
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'display_section',
            array(
                'label' => esc_html__('Display', 'foodmood-core'),
            )
        );

        $this->add_control(
            'hide_price',
            array(
                'label' => esc_html__('Hide Price?', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
            )
        );

        $this->add_control(
            'hide_raiting',
            array(
                'label' => esc_html__('Hide Raiting?', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'default' => 'yes',
            )
        );

        $this->add_control(
            'hide_content',
            array(
                'label' => esc_html__('Hide Content?', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
            )
        );

        $this->add_control(
            'content_letter_count',
            array(
                'label' => esc_html__('Characters Amount in Content', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'default' => '60',
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'hide_content' => '',
                ]
            )
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  CONTENT -> CAROUSEL OPTIONS
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'wgl_carousel_section',
            array(
                'label' => esc_html__('Carousel Options', 'foodmood-core'),
                'condition' => [
                    'products_layout' => 'carousel',
                ]
            )
        );

        $this->add_control(
            'autoplay',
            array(
                'label' => esc_html__('Autoplay', 'foodmood-core'),

                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
            )
        );

        $this->add_control(
            'autoplay_speed',
            array(
                'label' => esc_html__('Autoplay Speed', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'default' => '3000',
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'autoplay'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'use_pagination',
            array(
                'label' => esc_html__('Add Pagination control', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
            )
        );

        $this->add_control(
            'pag_type',
            array(
                'label' => esc_html__('Pagination Type', 'foodmood-core'),
                'type' => 'wgl-radio-image',
                'options' => [
                    'circle' => [
                        'title'=> esc_html__('Circle', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/pag_circle.png',
                    ],
                    'circle_border' => [
                        'title'=> esc_html__('Empty Circle', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/pag_circle_border.png',
                    ],
                    'square' => [
                        'title'=> esc_html__('Square', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/pag_square.png',
                    ],
                    'line' => [
                        'title'=> esc_html__('Line', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/pag_line.png',
                    ],
                    'line_circle' => [
                        'title'=> esc_html__('Line - Circle', 'foodmood-core'),
                        'image' => WGL_ELEMENTOR_ADDONS_URL . 'assets/img/wgl_composer_addon/icons/pag_line_circle.png',
                    ],
                ],
                'default' => 'circle',
                'condition' => [
                    'use_pagination'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'pag_offset',
            array(
                'label' => esc_html__('Pagination Top Offset', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'use_pagination'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'custom_pag_color',
            array(
                'label' => esc_html__('Custom Pagination Color', 'foodmood-core'),

                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
            )
        );

        $this->add_control(
            'pag_color',
            array(
                'label' => esc_html__('Color', 'foodmood-core'),
                'type' => Controls_Manager::COLOR,
                'default' => esc_attr($theme_color),
                'condition' => [
                    'custom_pag_color'  => 'yes',
                ]
            )
        );

        $this->add_control(
            'use_navigation',
            array(
                'label' => esc_html__('Add Navigation control', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
                'default' => 'yes',
            )
        );

        $this->add_control(
            'custom_resp',
            array(
                'label' => esc_html__('Customize Responsive', 'foodmood-core'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__('On', 'foodmood-core'),
                'label_off' => esc_html__('Off', 'foodmood-core'),
                'return_value' => 'yes',
            )
        );

        $this->add_control(
            'heading_desktop',
            array(
                'label' => esc_html__('Desktop Settings', 'foodmood-core'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'after',
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_medium',
            array(
                'label' => esc_html__('Desktop Screen Breakpoint', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'default' => '1025',
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_medium_slides',
            array(
                'label' => esc_html__('Slides to show', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'heading_tablet',
            array(
                'label' => esc_html__('Tablet Settings', 'foodmood-core'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'after',
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_tablets',
            array(
                'label' => esc_html__('Tablet Screen Breakpoint', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'default' => '800',
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_tablets_slides',
            array(
                'label' => esc_html__('Slides to show', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'heading_mobile',
            array(
                'label' => esc_html__('Mobile Settings', 'foodmood-core'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'after',
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_mobile',
            array(
                'label' => esc_html__('Mobile Screen Breakpoint', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'default' => '480',
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->add_control(
            'resp_mobile_slides',
            array(
                'label' => esc_html__('Slides to show', 'foodmood-core'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'step' => 1,
                'condition' => [
                    'custom_resp' => 'yes',
                ]
            )
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  Build Query Section
        /*-----------------------------------------------------------------------------------*/

        Wgl_Loop_Settings::init(
            $this,
            array(
                'post_type' => 'product',
                'hide_cats' => true,
                'hide_tags' => true
            )
        );


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> FILTER
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'filter_cats_style_section',
            array(
                'label' => esc_html__('Filter', 'foodmood-core'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'show_filter' => 'yes',
                ],
            )
        );

        $this->add_responsive_control(
            'filter_cats_padding',
            array(
                'label' => esc_html__('Filter padding', 'foodmood-core'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 5,
                    'left' => 30,
                    'right' => 30,
                    'bottom' => 5,
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .isotope-filter a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            )
        );

        $this->add_responsive_control(
            'filter_cats_margin',
            array(
                'label' => esc_html__('Filter Categories Margin', 'foodmood-core'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 0,
                    'left' => 0,
                    'right' => 5,
                    'bottom' => 15,
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .isotope-filter a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            )
        );

        $this->add_responsive_control(
            'filter_margin',
            array(
                'label' => esc_html__('Filter Margin', 'foodmood-core'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 0,
                    'left' => 0,
                    'right' => 0,
                    'bottom' => 20,
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .isotope-filter' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'custom_fonts_filter_cats',
                'selector' => '{{WRAPPER}} .isotope-filter a',
            )
        );


        $this->start_controls_tabs( 'filter_cats_color_tab' );

        $this->start_controls_tab(
            'custom_filter_cats_color_normal',
            array(
                'label' => esc_html__('Normal' , 'foodmood-core'),
            )
        );

        $this->add_control(
            'filter_cats_color',
            array(
                'label' => esc_html__('Color', 'foodmood-core'),
                'type' => Controls_Manager::COLOR,
                'default' => esc_attr($header_font_color),
                'selectors' => array(
                    '{{WRAPPER}} .isotope-filter a' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_control(
            'filter_cats_background',
            array(
                'label' => esc_html__('Background', 'foodmood-core'),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .isotope-filter a' => 'background: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'custom_filter_cats_color_hover',
            array(
                'label' => esc_html__('Active' , 'foodmood-core'),
            )
        );

        $this->add_control(
            'filter_cats_color_hover',
            array(
                'label' => esc_html__('Color', 'foodmood-core'),
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff',
                'selectors' => array(
                    '{{WRAPPER}} .isotope-filter a.active' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->add_control(
            'filter_cats_background_hover',
            array(
                'label' => esc_html__('Background', 'foodmood-core'),
                'type' => Controls_Manager::COLOR,
                'default' => esc_attr($theme_secondary_color),
                'selectors' => array(
                    '{{WRAPPER}} .isotope-filter a.active' => 'background: {{VALUE}};border-bottom-color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Border::get_type(),
            array(
                'name' => 'filter_cats_border',
                'label' => esc_html__('Border Type', 'foodmood-core'),
                'default' => '1px',
                'selector' => '{{WRAPPER}} .isotope-filter a',
            )
        );

        $this->add_control(
            'filter_cats_radius',
            array(
                'label' => esc_html__('Border Radius', 'foodmood-core'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'default' => [
                    'top' => 3,
                    'left' => 3,
                    'right' => 3,
                    'bottom' => 3,
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .isotope-filter a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            )
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            array(
                'name' => 'filter_cats_shadow',
                'selector' =>  '{{WRAPPER}} .isotope-filter a',
            )
        );

        $this->end_controls_section();


        /*-----------------------------------------------------------------------------------*/
        /*  STYLE -> ITEMS
        /*-----------------------------------------------------------------------------------*/
        $this->start_controls_section(
            'bg_style_section',
            array(
                'label' => esc_html__('Items', 'foodmood-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'background',
                'label' => esc_html__('Background', 'foodmood-core'),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .wgl-products .item .products-post',
            ]
        );

        $this->add_responsive_control(
            'item_gap',
            array(
                'label' => esc_html__('Gap Items', 'littledino-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'default' => [
                    'top' => 0,
                    'left' => 15,
                    'right' => 15,
                    'bottom' => 55,
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .wgl-products .item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .wgl-products' => 'margin-left: -{{LEFT}}{{UNIT}}; margin-right: -{{RIGHT}}{{UNIT}}; margin-bottom: -{{BOTTOM}}{{UNIT}}',
                ],
            )
        );

        $this->start_controls_tabs( 'items_border' );

        $this->start_controls_tab(
            'custom_headings_color_normal',
            array(
                'label' => esc_html__('Normal' , 'foodmood-core'),
            )
        );


        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'fields_options' => [
                    'border' => [
                        'default' => 'dashed',
                    ],
                    'width' => [
                        'default' => [
                            'top' => '2',
                            'right' => '2',
                            'bottom' => '2',
                            'left' => '2',
                            'isLinked' => false,
                        ],
                    ],
                    'color' => [
                        'default' => 'transparent',
                    ],
                ],
                'name' => 'bg_border_norm',
                'selector' => '{{WRAPPER}} .wgl-products .item .products-post:after',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'bg_shadow_norm',
                'selector' => '{{WRAPPER}} .wgl-products div .products-post:after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'custom_headings_color_hover',
            array(
                'label' => esc_html__('Hover' , 'foodmood-core'),
            )
        );


        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'fields_options' => [
                    'border' => [
                        'default' => 'dashed',
                    ],
                    'width' => [
                        'default' => [
                            'top' => '2',
                            'right' => '2',
                            'bottom' => '2',
                            'left' => '2',
                            'isLinked' => false,
                        ],
                    ],
                    'color' => [
                        'default' => $theme_color,
                    ],
                ],
                'name' => 'bg_border_hover',
                'selector' => '{{WRAPPER}} .wgl-products .item:hover .products-post:after',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'bg_shadow_hover',
                'selector' => '{{WRAPPER}} .wgl-products .item:hover .products-post:after',
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_control(
            'items_border_radius',
            [
                'label' => esc_html__('Border Radius', 'foodmood-core'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'default' => [
                    'top' => 20,
                    'right' => 20,
                    'bottom'=> 20,
                    'left'  => 20,
                    'unit'  => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} .wgl-products .item .products-post:after, {{WRAPPER}} .wgl-products .item .products-post img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }


    protected function render() {
        $atts = $this->get_settings_for_display();

        $products = new WglProducts();
        echo $products->render($atts);
    }

}