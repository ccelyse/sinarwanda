<?php
namespace WglAddons\Widgets;

use WglAddons\Includes\Wgl_Icons;
use WglAddons\Includes\Wgl_Carousel_Settings;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Control_Media;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Repeater;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Wgl_Combo_Menu extends Widget_Base {
    
    public function get_name() {
        return 'wgl-combo-menu';
    }

    public function get_title() {
        return esc_html__('Wgl Combo Menu', 'foodmood-core' );
    }

    public function get_icon() {
        return 'wgl-combo-menu';
    }
 
    public function get_categories() {
        return [ 'wgl-extensions' ];
    }

    public function get_script_depends() {
        return [
            'appear',
        ];
    }

    // Adding the controls fields for the premium title
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        $theme_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-custom-color'));
        $second_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-secondary-color'));
        $third_color = esc_attr(\Foodmood_Theme_Helper::get_option('theme-third-color'));
        $header_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('header-font')['color']);
        $main_font_color = esc_attr(\Foodmood_Theme_Helper::get_option('main-font')['color']);

        /*-----------------------------------------------------------------------------------*/
        /*  Content
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section('wgl_working_section',
            array(
                'label'         => esc_html__('Menu Content', 'foodmood-core'),
            )
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'thumbnail',
            array(
                'label'       => esc_html__( 'Image', 'foodmood-core' ),
                'type'        => Controls_Manager::MEDIA,
                'label_block' => true,
            )
        ); 

        $repeater->add_control(
            'menu_title',
            array(
                'label' => esc_html__('Title', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('Burger', 'foodmood-core'),
            )
        );  

        $repeater->add_control(
            'menu_desc',
            array(
                'label' => esc_html__('Description', 'foodmood-core'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => '',
            )
        );  

        $repeater->add_control(
            'menu_price',
            array(
                'label' => esc_html__('Price', 'foodmood-core'),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('$35', 'foodmood-core'),
                'separator' => 'after'
            )
        );  

        $repeater->add_control('link_item',
            array(
                'label'             => esc_html__('Link', 'foodmood-core'),
                'type'              => Controls_Manager::URL,
                'label_block' => true,
            )
        );

        $this->add_control(
            'items',
            array(
                'label'   => esc_html__( 'Menu', 'foodmood-core' ),
                'type'    => Controls_Manager::REPEATER,
                'default' => [
                    ['menu_title' => esc_html__('Burger', 'foodmood-core')],
                    ['menu_title' => esc_html__('Pizza', 'foodmood-core')],
                ],
                'fields'      => $repeater->get_controls(),
                'title_field' => '{{menu_title}}',
            )
        );

        $this->end_controls_section();   
        
        /*-----------------------------------------------------------------------------------*/
        /*  Style Section
        /*-----------------------------------------------------------------------------------*/

        $this->start_controls_section(
            'style_section',
            array(
                'label'     => esc_html__( 'Styles', 'foodmood-core' ),
                'tab'       => Controls_Manager::TAB_STYLE,
            )
        );

        $this->add_control(
            'item_styles',
            [
                'type' => Controls_Manager::HEADING,
                'label'   => esc_html__( 'Item Styles', 'foodmood-core' ),
            ]
        );

        $this->add_responsive_control(
			'item_margin',
			[
				'label' => esc_html__( 'Margin', 'foodmood-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .menu-item' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default'   => [
                    'top'       => 0,
                    'left'      => 0,
                    'right'     => 0,
                    'bottom'    => 26,
                ],
                'separator' => 'after'
			]
        );

        $this->add_control(
            'image_styles',
            [
                'type' => Controls_Manager::HEADING,
                'label'   => esc_html__( 'Image Styles', 'foodmood-core' ),
            ]
        );

        $this->add_responsive_control(
			'image_margin',
			[
				'label' => esc_html__( 'Margin', 'foodmood-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .menu-item_image-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default'   => [
                    'top'       => 0,
                    'left'      => 0,
                    'right'     => 20,
                    'bottom'    => 0,
                ],
                'separator' => 'after'
			]
        );
        
        $this->add_control(
            'title_styles',
            [
                'type' => Controls_Manager::HEADING,
                'label'   => esc_html__( 'Title Styles', 'foodmood-core' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'title_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .menu-item_title',
            )
        );

        $this->start_controls_tabs( 'title_color_tab' );

        $this->start_controls_tab(
            'custom_title_color_normal',
            array(
                'label' => esc_html__( 'Normal' , 'foodmood-core' ),
            )
        );

        $this->add_control(
            'title_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $header_font_color,
                'selectors' => array(
                    '{{WRAPPER}} .menu-item_title' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'custom_title_color_hover',
            array(
                'label' => esc_html__( 'Hover' , 'foodmood-core' ),
            )
        );

        $this->add_control(
            'title_color_hover',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => esc_attr($header_font_color),
                'selectors' => array(
                    '{{WRAPPER}} .menu-item:hover .menu-item_title' => 'color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();   
        
        $this->add_responsive_control(
			'title_margin',
			[
				'label' => esc_html__( 'Margin', 'foodmood-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .menu-item_title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
                'separator' => 'after'
			]
        );
        
        $this->add_control(
            'desc_styles',
            [
                'type' => Controls_Manager::HEADING,
                'label'   => esc_html__( 'Description Styles', 'foodmood-core' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'desc_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .menu-item_desc',
            )
        );

        $this->add_control(
            'desc_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#a0a0a0',
                'selectors' => array(
                    '{{WRAPPER}} .menu-item_desc' => 'color: {{VALUE}};',
                ),
            )
        );
        
        $this->add_responsive_control(
			'desc_margin',
			[
				'label' => esc_html__( 'Margin', 'foodmood-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .menu-item_desc' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
                'separator' => 'after'
			]
		);

        $this->add_control(
            'price_styles',
            [
                'type' => Controls_Manager::HEADING,
                'label'   => esc_html__( 'Price Styles', 'foodmood-core' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name' => 'price_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .menu-item_price',
            )
        );

        $this->add_control(
            'price_color',
            array(
                'label' => esc_html__( 'Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .menu-item_price' => 'color: {{VALUE}};',
                ),
            )
        );
        
        $this->add_responsive_control(
			'price_margin',
			[
				'label' => esc_html__( 'Margin', 'foodmood-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .menu-item_price' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
                'separator' => 'after'
			]
        );
        
        $this->add_control(
            'sep_color',
            array(
                'label' => esc_html__( 'Separator Between Color', 'foodmood-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => $theme_color,
                'selectors' => array(
                    '{{WRAPPER}} .menu-item_content:after' => 'border-color: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_section(); 

    }

    protected function render() {

        $settings = $this->get_settings_for_display();

        $this->add_render_attribute( 'combo-menu', [
			'class' => [
                'wgl-combo-menu',
            ],
        ] );

        ?><div <?php echo $this->get_render_attribute_string( 'combo-menu' ); ?>><?php

        foreach ( $settings['items'] as $index => $item ) {

            if ( !empty( $item['link_item']['url'] ) ) {
                $link_item = $this->get_repeater_setting_key( 'link_item', 'list' , $index ); 
                $this->add_render_attribute( $link_item, [
                    'class' => ['menu-item menu-item_link'],
                    'href' => esc_url($item['link_item']['url'] ),
                    'target' => $item['link_item']['is_external'] ? '_blank' : '_self',
                    'rel' => $item['link_item']['nofollow'] ? 'nofollow' : '',
                ] );
            }

            $menu_image = $this->get_repeater_setting_key( 'thumbnail', 'list' , $index ); 
            $this->add_render_attribute( $menu_image, [
                'class' => 'main_image',
                'src' => esc_url($item['thumbnail']['url']),
                'alt' => Control_Media::get_image_alt( $item['thumbnail'] ),
            ] );

            $menu_title = $this->get_repeater_setting_key( 'menu_title', 'items' , $index ); 
            $this->add_render_attribute( $menu_title, [
                'class' => [
                    'menu-item_title',
                ],
            ] );

            $menu_price = $this->get_repeater_setting_key( 'menu_price', 'items' , $index ); 
            $this->add_render_attribute( $menu_price, [
                'class' => [
                    'menu-item_price',
                ],
            ] );

            if ( !empty($item['link_item']['url']) ) {
                ?><a <?php echo $this->get_render_attribute_string( $link_item ); ?>><?php
            } else {?>
                <div class="menu-item"><?php
            }
                if (!empty($item['thumbnail']['url'])) {?>
                    <div class="menu-item_image-wrap"><img <?php echo $this->get_render_attribute_string( $menu_image ); ?> /></div><?php
                }?>
                <div class="menu-item_content-wrap">
                    <div class="menu-item_content"><?php
                        if (!empty($item['menu_title'])) {
                            ?><div <?php echo $this->get_render_attribute_string( $menu_title ); ?>><?php echo esc_html($item['menu_title']); ?></div><?php
                        }
                        if (!empty($item['menu_price'])) {
                            ?><div <?php echo $this->get_render_attribute_string( $menu_price ); ?>><?php echo esc_html($item['menu_price']); ?></div><?php
                        }?>
                    </div><?php
                    if (!empty($item['menu_desc'])) {
                        ?><div class="menu-item_desc"><?php echo esc_html($item['menu_desc']); ?></div><?php
                    }?>
                </div><?php
            if ( !empty($item['link_item']['url']) ) {?>
                </a><?php
            } else {?>
                </div><?php
            }
        }

        ?></div><?php 

    }
    
}