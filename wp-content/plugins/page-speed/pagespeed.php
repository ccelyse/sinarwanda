<?php

/**

 * Plugin Name: Page Speed

 * Plugin URI: http://gutenframestudio.com/product/page-speed-pro/

 * Description: Page Speed Plugin help you to get a passing grade in Google Page Speed Insight.

 * Version: 1.3.4

 * Author: Rex Anthony D. Eubanas (Batugan Design)

 * Author URI: https://profiles.wordpress.org/buboiasinine

 * License: A "Slug" license name e.g. GPL2

 */

 

 /*  Copyright � 2019  Rex_Anthony_D_Eubanas  (email : batugandesign@gmail.com)

 This program is free software; you can redistribute it and/or modify

    it under the terms of the GNU General Public License, version 2, as 

    published by the Free Software Foundation.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program; if not, write to the Free Software

    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


 require_once('lib/function/functions.php');
 require_once('lib/function/settings.php');

 require_once('lib/function/cache_policy.php');
require_once('lib/function/enable_text_compresion.php');


 require_once('lib/function/render-blocking-resources-stylesheet.php');
  require_once('lib/function/render-blocking-resources-javascript.php');



require_once('lib/function/properly_size_images.php');
require_once('lib/function/defer_offscreen_images.php');
require_once('lib/function/efficiently_encode_images.php');



?>