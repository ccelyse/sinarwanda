<?php

function ps_pagespeed_efficiently_encode_images_set_quality( $quality ) {
    $ps_pagespeed = get_option("ps_pagespeed");
    if ($ps_pagespeed['efficiently_encode_images']['jpg_quality'] == "on") {
    return 80;
    }
}
add_filter( 'jpeg_quality', 'ps_pagespeed_efficiently_encode_images_set_quality' );
add_filter( 'wp_editor_set_quality', 'ps_pagespeed_efficiently_encode_images_set_quality' );

add_filter('wp_handle_upload', 'ps_pagespeed_efficiently_encode_images_handle_upload');
function ps_pagespeed_efficiently_encode_images_handle_upload($data) {
	$ps_pagespeed = get_option("ps_pagespeed");
	if ($ps_pagespeed['efficiently_encode_images']['jpg_quality'] == "on") {
        if( ! isset( $data['file'] ) || ! isset( $data['type'] ) )
        return $data;
        // Check for a valid image editor
        $editor = wp_get_image_editor( $data['file'] );    
        if( ! is_wp_error( $editor ) )   {
            // Set the new image quality
           // $old_size = $editor->get_size();
          //  $reduce_size =  $old_size['height']  - 10;
         //   $editor->resize( NULL,   $reduce_size, true );
             $editor->set_quality( 80 );
			$editor->save( $data['file'] );
                
        }
    }  
    return $data;
}
function ps_pagespeed_efficiently_encode_images_generate_attachment_metadata($metadata, $attachment_id) {
    $ps_pagespeed = get_option("ps_pagespeed");
    if ($ps_pagespeed['efficiently_encode_images']['jpg_quality'] == "on") {
	$file = get_attached_file( $attachment_id );
    $type = get_post_mime_type( $attachment_id );
        // Check for a valid image editor
        $editor = wp_get_image_editor( $file );
        if( ! is_wp_error( $editor ) )  {
            // Set the new image quality
           //  $old_size = $editor->get_size();
           // $reduce_size =  $old_size['height']  - 10;
           // $editor->resize( NULL,   $reduce_size, true );
             $editor->set_quality( 80 );
           $editor->save( $file );

                
        }
    }
    return $metadata;
}

?>