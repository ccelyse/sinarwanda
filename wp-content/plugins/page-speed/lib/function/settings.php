<?php
add_action( 'admin_init', 'ps_pagespeed_register_settings' );
function ps_pagespeed_register_settings() {
	
	register_setting( 'ps_pagespeedpro_settings_group', 'ps_pagespeed' );
	
	
}
function ps_pagespeed_general_settings() {
?>	

<style>
    .box-container {
    display: flex;
    flex-wrap: wrap;
}
.box.box-left {
    flex-basis: 70%;
    background: #fff;
    padding: 20px;
    margin: 20px 0px;
        box-sizing: border-box;
}
.box.box-right {
    flex-basis: 30%;
    box-sizing: border-box;
    margin: 10px auto;
    padding: 20px;
}
.pagespeed-accordion > h3 {
    border-top: 1px solid #ddd;
    padding: 10px;
    margin: 0 auto;
        outline: none;
            cursor: pointer;
}
.pagespeed-accordion > h3:hover {
    background: #f1f1f1;
}
.pagespeed-accordion .ui-accordion-content {
    padding: 0px 10px 20px;
}
.pagespeed-accordion h3 .dashicons, .pagespeed-accordion h3 .dashicons-before:before {
    float: right;
}
textarea.inline-css {
    width: 100%;
    height: 200px;
}
input[type="text"] {
    width: 100%;
}
td.td-handel {
    width: 20%;
}
td.td-url {
    width: 80%;
}
</style>

<div class="box-wrap">
    <div class="box-container">
        
        
        <div class="box box-left">
            
             <form method="post" action="options.php">

    <?php settings_fields( 'ps_pagespeedpro_settings_group' ); ?>
    <?php do_settings_sections( 'ps_pagespeedpro_settings_group' ); ?>

<h1> PageSpeed </h1> 	


<h2>  These optimizations can speed up your page load. </h2> 


<?php $ps_pagespeed = get_option("ps_pagespeed"); ?>



<div class="pagespeed-accordion">
    
       
  
  
    
        <h3> Enable text compression </h3>
  <div>
    <p> Text-based resources should be served with compression (gzip, deflate or brotli) to minimize total network bytes. <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a> </p>
	
	<p> 
	    <input type="checkbox" name="ps_pagespeed[enable_text_compression]" value="on" <?php echo $ps_pagespeed['enable_text_compression']=='on' ? 'checked="checked"' : '' ?> />
	    <b> Enable text compression in your web server configuration. </b> 
	</p>
	
	<p> <b> NOTE: </b> Please make sure to resave your permalinks after activating. See <a href="https://wordpress.org/plugins/page-speed/faq/" target="_blank"> FAQ </a> for more details. </p>
	
	
  </div>
  
   <h3> Serve static assets with an efficient cache policy  </h3>
  <div>
    <p> A long cache lifetime can speed up repeat visits to your page.  <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a> </p>
	
	<p> <input type="checkbox" name="ps_pagespeed[cache_policy]" value="on" <?php echo $ps_pagespeed['cache_policy']=='on' ? 'checked="checked"' : '' ?> />
	
	<b> Set Expiry date or a maximum age in the HTTP headers. </b> </p> 
	
	<p> <b> NOTE: </b>  Please make sure to resave your permalinks after activating. See <a href="https://wordpress.org/plugins/page-speed/faq/" target="_blank"> FAQ </a> for more details. </p>
	
	
  </div>
  
    
    <h3> Eliminate render-blocking resources </h3>
    
    <div>
        
         <p> Resources are blocking the first paint of your page. Consider delivering critical JS/CSS inline and deferring all non-critical JS/styles.  <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a>   </p>
         
         
         
          <h4> Render Blocking Javascript </h4>
          
           <p> <input type="radio" name="ps_pagespeed[render_blocking_resources][javascript]" value=""  <?php if ( '' == $ps_pagespeed['render_blocking_resources']['javascript'] ) echo 'checked="checked"'; ?>  />  Default  </p>
		<p> <input type="radio" name="ps_pagespeed[render_blocking_resources][javascript]" value="async"  <?php if ( 'async' == $ps_pagespeed['render_blocking_resources']['javascript'] ) echo 'checked="checked"'; ?>  /> Add <b> async </b> attribute   </p> 
    	<p>  <input type="radio" name="ps_pagespeed[render_blocking_resources][javascript]" value="defer"  <?php if ( 'defer' == $ps_pagespeed['render_blocking_resources']['javascript'] ) echo 'checked="checked"'; ?>  /> Add <b> defer </b> attribute  </p>
	
          
          
        
        <h4> Render Blocking Stylesheet </h4>
        
        <p> <input type="checkbox" name="ps_pagespeed[render_blocking_resources][stylesheet]" value="defer"  <?php if ( 'defer' == $ps_pagespeed['render_blocking_resources']['stylesheet'] ) echo 'checked="checked"'; ?>  />  Defered  </p>
        
   <h4> Inline Critical CSS </h4>
	<a href="https://pegasaas.com/critical-path-css-generator/" target="_blank">  Generate Critical Assets  </a>  Copy and Paste Code Here 
	<p> <textarea name="ps_pagespeed[render_blocking_resources][critical_css]" class="inline-css"> <?php echo $ps_pagespeed['render_blocking_resources']['critical_css']; ?>   </textarea> </p>
    

	
    </div>
    
    
   <h3> Properly Size Images  </h3>
  <div>
      
     <?php $optimize_images = get_option("optimize_images"); ?>
      
    <p>  Serve images that are appropriately-sized to save cellular data and improve load time.   <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a>   </p>
    
    <p>  <input type="checkbox" name="ps_pagespeed[properly_size_images][srcset_img]" value="on" <?php echo $ps_pagespeed['properly_size_images']['srcset_img'] =='on' ? 'checked="checked"' : '' ?> />   Properly resize images using srcset attribute.  </p>
    <p>  <input type="checkbox" name="ps_pagespeed[properly_size_images][srcset_bg]" value="on" <?php echo $ps_pagespeed['properly_size_images']['srcset_bg'] =='on' ? 'checked="checked"' : '' ?> />   Properly resize background images using srcset attribute.  </p>
    
    <p>  <input type="checkbox" name="ps_pagespeed[properly_size_images][responsive_images]" value="on" <?php echo $ps_pagespeed['properly_size_images']['responsive_images'] =='on' ? 'checked="checked"' : '' ?> />   Add addiotnal responsive images  </p>
    
    
  </div>
  
  <h3> Defer Offscreen Images  </h3>
  <div>
    <h4>   Consider lazy-loading offscreen and hidden images after all critical resources have finished loading to lower time to interactive.   <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a>  </h4>
    
    <p>  <input type="checkbox" name="ps_pagespeed[defer_offscreen_images][lazyload_img]" value="on" <?php echo $ps_pagespeed['defer_offscreen_images']['lazyload_img'] =='on' ? 'checked="checked"' : '' ?> />   Lazy Load Images  </p>
    <p>  <input type="checkbox" name="ps_pagespeed[defer_offscreen_images][lazyload_bg]" value="on" <?php echo $ps_pagespeed['defer_offscreen_images']['lazyload_bg'] =='on' ? 'checked="checked"' : '' ?> />   Lazy Background Images  </p>
  
    
    
  </div>
  
  
  <h3> Efficiently encode images  </h3>
   <div>
     <h4> Optimized images load faster and consume less cellular data.  <a href="https://wordpress.org/plugins/page-speed/" target="_blank"> Learn More </a>  </h4>
     
      <p>  <input type="checkbox" name="ps_pagespeed[efficiently_encode_images][jpg_quality]" value="on" <?php echo $ps_pagespeed['efficiently_encode_images']['jpg_quality'] =='on' ? 'checked="checked"' : '' ?> />   Compress JPG images by 80%. </p>
     
     
   </div>
    
    


</div>



 <?php submit_button(); ?>


</form>
            
        </div>
        
        <div class="box box-right">
            
            <center> 
            <h3>  Help us to improve this plugin. </h3>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="K88RFNMVRRNCA" />
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_PH/i/scr/pixel.gif" width="1" height="1" />
</form>
</center>


        </div>
        
       


    </div>
</div>



<script>
    jQuery( document ).ready(function($) {
	  $( ".pagespeed-accordion" ).accordion({
	       icons: { "header": "dashicons dashicons-arrow-down", "activeHeader": "dashicons dashicons-arrow-up" },
      heightStyle: "content",
	    active: 100
    });
});

</script>




<?php
}
?>