<?php


 add_action( 'wp_enqueue_scripts', 'ps_pagespeed_defer_offscreen_images', 2, 2 );
function ps_pagespeed_defer_offscreen_images() {

 if ( !is_user_logged_in() ) { 
 
  $ps_pagespeed = get_option("ps_pagespeed");
  
  ?>
  <?php if ($ps_pagespeed['defer_offscreen_images']['lazyload_bg'] == "on") { ?>
  <style> .lazy-background, .lazy-background * { background-image: none !important; } </style>
  <?php } ?>
  
  <script>
  	<?php if ($ps_pagespeed['defer_offscreen_images']['lazyload_img'] == "on") { ?>
   document.addEventListener("DOMContentLoaded", function() {
      var img = document.querySelectorAll("img");
      img.forEach.call(img, function(element, index, array) {
           var src =  element.getAttribute("src");
           var srcset =  element.getAttribute("srcset");
           var placeholder = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
          element.classList.add("lazy");
          element.setAttribute("data-src", src);
          element.setAttribute("data-srcset", srcset);
          element.setAttribute("src", placeholder);
          element.setAttribute("srcset", placeholder);
          
           var srcset_attr = ""+src+" 1x, "+src+" 2x";
          
          if (!srcset ) {
                element.setAttribute("data-srcset", srcset_attr);
            }
            
            var width = element.clientWidth;
            
          
      });
   });
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove("lazy");
          lazyImage.classList.add("visible");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {
    // Possibly fall back to a more compatible method here
  }
});
<?php } ?>



<?php if ($ps_pagespeed['defer_offscreen_images']['lazyload_bg'] == "on") { ?>
document.addEventListener("DOMContentLoaded", function() {
      var bg = document.querySelectorAll("body *");
      bg.forEach.call(bg, function(element, index, array) {
          
           var bgImg=   window.getComputedStyle(element).backgroundImage;
        var bgUrl = bgImg.replace(/(url\(|\)|")/g, "");
          
          var background = "background-image:"+bgImg+";";
          
         
          
          if ( bgImg !== "none" ) {
               
              element.classList.add("lazy-background");
            element.setAttribute("background", background);
            element.style.backgroundImage = "none";
          }
          
          
      });
   });


document.addEventListener("DOMContentLoaded", function() {
  var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

  if ("IntersectionObserver" in window) {
    let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
            
            var background =  entry.target.getAttribute("background");
            
          entry.target.classList.add("visible");
          entry.target.classList.remove("lazy-background");
          entry.target.setAttribute("style", background);
          lazyBackgroundObserver.unobserve(entry.target);
        }
      });
    });

    lazyBackgrounds.forEach(function(lazyBackground) {
      lazyBackgroundObserver.observe(lazyBackground);
    });
  }
});
<?php } ?>



  
  </script>
  
  <?php
 
 }


}

?>