<?php
//  add admin menu
add_action('admin_menu', 'pg_pagespeed_menu');

function pg_pagespeed_menu() {

// Add a new top-level menu (ill-advised):
    add_menu_page(__('PageSpeed','pagespeed_menu'), __('PageSpeed','pagespeed_menu'), 'manage_options', 'pagespeed-settings', 'ps_pagespeed_general_settings', 'dashicons-chart-area' );

// Add a submenu to the custom top-level menu:
   //  add_submenu_page('pagespeed-settings', __('PageSpeed PRO','pagespeed_menu'), __('PageSpeed PRO','pagespeed_menu'), 'manage_options', 'pagespeed-pro-settings', 'ps_pagespeed_pro_settings');	

}

// register admin styles and scripts
add_action( 'admin_enqueue_scripts', 'pg_pagespeed_admin_scripts' );  
function pg_pagespeed_admin_scripts($hook) {
	
	wp_register_script( 'pagespeed-admin-script',  plugins_url( ) . '/page-speed/lib/js/admin-script.js' );
	wp_enqueue_script( 'pagespeed-admin-script' );
	
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jquery-ui-accordion' );
	
}



// minify css
function pagespeed_minify_css($css) {
  // some of the following functions to minimize the css-output are directly taken
  // from the awesome CSS JS Booster: https://github.com/Schepp/CSS-JS-Booster
  // all credits to Christian Schaefer: http://twitter.com/derSchepp
  // remove comments
  $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
  // backup values within single or double quotes
  preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
  for ($i=0; $i < count($hit[1]); $i++) {
    $css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);
  }
  // remove traling semicolon of selector's last property
  $css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);
  // remove any whitespace between semicolon and property-name
  $css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);
  // remove any whitespace surrounding property-colon
  $css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);
  // remove any whitespace surrounding selector-comma
  $css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);
  // remove any whitespace surrounding opening parenthesis
  $css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);
  // remove any whitespace between numbers and units
  $css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);
  // shorten zero-values
  $css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);
  // constrain multiple whitespaces
  $css = preg_replace('/\p{Zs}+/ims',' ', $css);
  // remove newlines
  $css = str_replace(array("\r\n", "\r", "\n"), '', $css);
  // Restore backupped values within single or double quotes
  for ($i=0; $i < count($hit[1]); $i++) {
    $css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);
  }
  return $css;
}


function ps_pagespeed_default_option() {
	$option_name =  'ps_pagespeed';
    $new_value =  array(
    	       "enable_text_compression" =>  "on",
    	       "cache_policy" =>  "on",
    	       "render_blocking_resources" =>  array(
    	            "javascript" => "async",
    	            "stylesheet" => "defer",
    	           ),
    	       "properly_size_images" =>  array(
    	            "srcset_img" => "on",
    	            "srcset_bg" => "on",
    	            "responsive_images" => "on",
    	           ),
    	       "defer_offscreen_images" =>  array(
    	            "lazyload_img" => "on",
    	            "lazyload_bg" => "on",
    	           ),
    	       "efficiently_encode_images" =>  array(
    	            "jpg_quality" => "on",
    	           ),
    	    );
     
    if ( get_option( $option_name ) !== false ) {
     
        // The option already exists, so update it.
       // delete_option( $option_name, $new_value );
     
    } else {
     
        // The option hasn't been created yet, so add it with $autoload set to 'no'.
        $deprecated = null;
        $autoload = 'no';
        add_option( $option_name, $new_value, $deprecated, $autoload );
    }
}
 add_action( 'init', 'ps_pagespeed_default_option' );
 
 
// beta  testing 



// reder blocking stylesheet

add_action( 'style_loader_tag', 'ps_pagespeed_get_frontend_stylesheets' );  
function ps_pagespeed_get_frontend_stylesheets( $html ) {
    
     if ( !is_admin()  )  {
global $wp_styles;
$ps_styles = get_option( 'ps_styles' );
$get_styles = array();

    foreach( $wp_styles->queue as $wp_key => $wp_handel) {
		    
		    $get_styles[$wp_handel]['handel'] = $wp_handel;
	        $get_styles[$wp_handel]['url']  = $wp_styles->registered[$wp_handel]->src;
		    $get_styles[$wp_handel]['action']  = $ps_styles[$wp_handel]['action'];
	}

$option_name = 'ps_styles';
$option_value = $get_styles;

// add or update the  list 
if ( $ps_styles !== false ) {
    // The option already exists, so we just update it.
    
   update_option( $option_name, $option_value );
} else {
    // The option hasn't been added yet. We'll add it with $autoload set to 'no'.
    $deprecated = null;
    $autoload = 'yes';
   add_option( $option_name, $option_value, $deprecated, $autoload );
}

}

return $html;

}







?>