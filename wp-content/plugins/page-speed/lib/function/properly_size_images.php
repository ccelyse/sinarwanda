<?php
add_action( 'wp_enqueue_scripts', 'ps_pagespeed_properly_size_images', 1, 1 );
function ps_pagespeed_properly_size_images() {
    
    
    if ( !is_user_logged_in() ) { 
        
        $ps_pagespeed = get_option("ps_pagespeed");
        
        ?>
        
         
  
        
        <script> 
        document.addEventListener("DOMContentLoaded", function() {
	
	<?php if ($ps_pagespeed['properly_size_images']['srcset_img'] == "on") { ?>
	    var img = document.querySelectorAll("img");
        img.forEach.call(img, function(element, index, array) {
            
            var src =  element.getAttribute("src");
            var srcset =  element.getAttribute("srcset");
            var sizes =  element.getAttribute("sizes");
            var width = element.clientWidth;
             
            var srcset_attr = ""+src+" 1x ";
            var sizes_attr = "(max-width: 480px) 100vw, (max-width: 1024px) 50vw, 800px";
            
            if (!srcset ) {
                element.setAttribute("srcset", srcset_attr);
            }
           element.setAttribute("sizes", width+"px");
        });
        window.addEventListener("resize", imgSizes);
        window.removeEventListener("orientationchange", imgSizes);
        
        function imgSizes() {
            var img = document.querySelectorAll("img");
            img.forEach.call(img, function(element, index, array) {
                
                var width = element.clientWidth;
                
               element.setAttribute("sizes", width+"px");
            });
        }
	<?php   } ?>
    
   
    
    <?php if ($ps_pagespeed['properly_size_images']['srcset_bg'] == "on") { ?>
    var bg = document.querySelectorAll("body *");
    bg.forEach.call(bg, function(element, index, array) {
        
        var bgImg=   window.getComputedStyle(element).backgroundImage;
        var bgUrl = bgImg.replace(/(url\(|\)|")/g, "");
        
        var imageUrl = "background-image: url("+bgUrl+");";
        
        <?php if ($ps_pagespeed['properly_size_images']['responsive_images'] == "on") : ?>
        
            var bgDesktop = bgUrl.replace('.jpg', '-1350x940.jpg');
            var bgMobile = bgUrl.replace('.jpg', '-412x660.jpg');
        
            var imageSet = "background-image: image-set( url("+bgUrl+") 1x, url("+bgDesktop+") 2x, url("+bgMobile+") 3x );";
            var webkitImageSet = "background-image: -webkit-image-set( url("+bgUrl+") 1x, url("+bgDesktop+") 2x, url("+bgMobile+") 3x );";
            
            if ( bgImg !== "none" ) {
            
                 element.setAttribute("style", imageUrl);
                element.setAttribute("style", imageSet);
                element.setAttribute("style", webkitImageSet); 
            }
        
        <?php  else: ?>
            
             if ( bgImg !== "none" ) {
                 element.setAttribute("style", imageUrl);
            }
        
        <?php  endif; ?>
        
        
        
       
    });
    <?php   } ?>
    
    
});
        </script>
        
        <?php  
        
    }
    
  

}


add_action( 'after_setup_theme', 'ps_pagespeed_properly_size_images_add_image' );
function ps_pagespeed_properly_size_images_add_image() {
    $ps_pagespeed = get_option("ps_pagespeed");
    
     if ($ps_pagespeed['properly_size_images']['responsive_images'] == "on") { 
          add_image_size( 'thumbnail-resize', 150 ); // 150 pixels wide (and unlimited height)
          add_image_size( 'thumbnail-crop', 150, 150, true ); // (cropped)
          
          add_image_size( 'background-mobile', 412, 660, true ); // 150 pixels wide (and unlimited height)
          add_image_size( 'background-desktop', 1350, 940, true ); // 150 pixels wide (and unlimited height)
          
     }
   
}


?>