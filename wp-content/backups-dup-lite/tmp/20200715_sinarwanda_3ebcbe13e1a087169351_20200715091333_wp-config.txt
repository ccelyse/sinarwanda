<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', '' );


/** MySQL database username */

define( 'DB_USER', '' );


/** MySQL database password */

define( 'DB_PASSWORD', '' );


/** MySQL hostname */

define( 'DB_HOST', '' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         ';S^e0&~$XQ0iT_^wLfYk~_tjLK8M!wow~L|,W# 28SSuW|g|T; JMOz^{/Qcy+H`' );

define( 'SECURE_AUTH_KEY',  'Fkqcx@&tdh$FR`5JJ4~mLK`Mdj,1+:prb?YJd:n=B*66)vQDB#wt5nOC_5ttBR`?' );

define( 'LOGGED_IN_KEY',    'XR##_ZzwyE^U@*dR0[[Z%]>M;&Bk,reoXC$U:Z+v}Kxvj}d=G?!:6w@TAxPXgj)x' );

define( 'NONCE_KEY',        '4F{S1z/ E;7m%a0=PVt/9J%HYM X};p[wfZ& InMmWyHu[NWgPYy<G;k}{3*Lb&}' );

define( 'AUTH_SALT',        '+:C~[}0q|Y`!8.}Y>cZa>5}k.l4dPcsINT._1A`k|ORH)|d)2bX{%Rr>F#pKD0uO' );

define( 'SECURE_AUTH_SALT', 'Xw51 ogGMkrxSXNQ UjI@T3~)_4?_2,N-[-]pY g?v@D^c0@o& ($VDM-X{Gut=E' );

define( 'LOGGED_IN_SALT',   'W9;4;~[P;b~CQU<!#OfTxk~f*`nao>ytL=R*tG-^MPuZl+e2a,K)}%ctvFA+`L3Z' );

define( 'NONCE_SALT',       'OjOYOP6^NwqAE$Al+iDwI 5Zxq`!3pLMP2w)yPma3Gvn*DLXh,nX@.=l;Bzu_4Az' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

